package com.mycompany.managefactory.technique;

import com.mycompany.managefactory.controleur.Controleur;
import com.mycompany.managefactory.modele.*;
import com.opencsv.CSVReader;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Classe qui implémente l'interface DonneesStrategy : source de données CSV
 */
public class CSV_file implements DonneesStrategy {

    /**
     * Retourne les Elements chargés depuis la source de données CSV
     * @return Retourne une collection observable d'objet Elements
     * @throws Exception Erreur lors du chargement des Elements
     */
    @Override
    public ObservableList<Element> loadElement() throws Exception {
        FileChooser choixFichier = new FileChooser();

        // Impose le choix d'un fichier CSV à l'utilisateur (pour le moment l'appli ne prend en compte que ce genre de fichier)
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv");
        choixFichier.getExtensionFilters().add(extFilter);

        // Ouvre la fenêtre de dialogue pour déterminer le chemin d'enregistrement du fichier
        File fichier = choixFichier.showOpenDialog(Controleur.primaryStage);
        
        // Collection qui sera retournée
        ObservableList<Element> listeElement = FXCollections.observableArrayList();

        if (fichier != null) {
            // Parcours des lignes du fichier
            for (String[] l : this.getLignesFichier(fichier.getPath())) {
                Element e;

                // L'élément est un ELEMENT si prix de vente = NA et que prix achat != NA
                if (l[5].contains("NA") && !l[4].contains("NA")) {
                    e = new Element(l[0], l[1], Integer.parseInt(l[2]), this.getUnite(l[3]), Double.parseDouble(l[4]));
                }
                // L'élément est un produit si il a un prix de vente
                else {
                    // Si le produit contient un prix d'achat et un prix de vente
                    if (l[4].contains("NA") && !l[5].contains("NA")) {
                        e = new Produit(Double.parseDouble(l[5]), l[0], l[1], Integer.parseInt(l[2]), this.getUnite(l[3]), 0);
                    }
                    else {
                        if (l[4].contains("NA") && l[5].contains("NA")) {
                            e = new Produit(0, l[0], l[1], Integer.parseInt(l[2]), this.getUnite(l[3]), 0);
                        }
                        else {
                            e = new Produit(Double.parseDouble(l[5]), l[0], l[1], Integer.parseInt(l[2]), this.getUnite(l[3]), Double.parseDouble(l[4]));
                        }
                    }

                }
                listeElement.add(e);
            }
        }
        return listeElement;
    }


    /**
     * Retourne les Chaines de Production chargées depuis la source de données CSV
     * @return Retourne une collection observable d'objet ChaineProd
     * @throws Exception Erreur lors du chargement des Chaines
     */
    @Override
    public ObservableList<ChaineProd> loadChaine() throws Exception {
        FileChooser choixFichier = new FileChooser();

        // Impose le choix d'un fichier CSV à l'utilisateur (pour le moment l'appli ne prend en compte que ce genre de fichier)
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv");
        choixFichier.getExtensionFilters().add(extFilter);

        // Ouvre la fenêtre de dialogue pour déterminer le chemin d'enregistrement du fichier
        File fichier = choixFichier.showOpenDialog(Controleur.primaryStage);
        
        // Collection qui sera retournée
        ObservableList<ChaineProd> listeChaine = FXCollections.observableArrayList();

        if (fichier != null) {
            // Variables pour construire un objet chaine
            String code, nom;
            int niveau, temps, pQualifie, pNonQualifie;
            HashMap<Element, Integer> listeEntree;
            HashMap<Produit, Integer> listeSortie;

            // Parcours des lignes du fichier (de chaque chaine inscrite dans le fichier)
            for (String[] l : this.getLignesFichier(fichier.getPath())) {
                code = l[0];
                nom = l[1];
                niveau = Integer.parseInt(l[4]);
                temps = Integer.parseInt(l[5]);
                pNonQualifie = Integer.parseInt(l[6]);
                pQualifie = Integer.parseInt(l[7]);

                // Chargement des HashMap
                listeEntree = this.getMapChaineInputOutputElement(Controleur.listeStock, l[2]);
                listeSortie = this.getMapChaineInputOutputProduit(Controleur.listeStock, l[3]);

                // Création de l'objet chaine
                ChaineProd c = new ChaineProd(code, nom, niveau, temps, pQualifie, pNonQualifie, listeEntree, listeSortie);

                // Ajout de l'objet à la collection
                listeChaine.add(c);
            }
        }

        return  listeChaine;
    }


    /**
     * Retourne les Demandes chargées depuis la source de données CSV
     * @return Retourne une collection observable d'objet Demande
     * @throws Exception Erreur lors du chargement des demandes
     */
    @Override
    public ObservableList<Demande> loadDemande() throws Exception {
        FileChooser choixFichier = new FileChooser();

        // Impose le choix d'un fichier CSV à l'utilisateur (pour le moment l'appli ne prend en compte que ce genre de fichier)
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv");
        choixFichier.getExtensionFilters().add(extFilter);

        // Ouvre la fenêtre de dialogue pour déterminer le chemin d'enregistrement du fichier
        File fichier = choixFichier.showOpenDialog(Controleur.primaryStage);
        
        // Collection qui sera retournée
        ObservableList<Demande> listeDemande = FXCollections.observableArrayList();

        if (fichier != null) {
            // Parcours des lignes du fichier
            for (String[] l : this.getLignesFichier(fichier.getPath())) {
                if (!l[1].equals("0")) {
                    Demande d = new Demande(this.getElementByCode(Controleur.listeStock, l[0]), Integer.parseInt(l[1]));
                    listeDemande.add(d);
                }
            }
        }

        return listeDemande;
    }


    /**
     * Retourne le personnel de l'usine chargé depuis la source de données CSV
     * @return Retourne une collection observable d'objet Personnel
     * @throws Exception Erreur lors du chargement du personnel
     */
    @Override
    public ObservableList<Personnel> loadPersonnel() throws Exception {
        FileChooser choixFichier = new FileChooser();

        // Impose le choix d'un fichier CSV à l'utilisateur (pour le moment l'appli ne prend en compte que ce genre de fichier)
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv");
        choixFichier.getExtensionFilters().add(extFilter);

        // Ouvre la fenêtre de dialogue pour déterminer le chemin d'enregistrement du fichier
        File fichier = choixFichier.showOpenDialog(Controleur.primaryStage);


        // Collection qui sera retournée
        ObservableList<Personnel> listePersonnel = FXCollections.observableArrayList();

        if (fichier != null) {
            // Variable pour construire un objet Personnel
            int num, nbHeures;
            Qualification qualification;
            HashMap<ChaineProd, Integer> planning;


            // Parcours des lignes du fichier
            for (String[] l : this.getLignesFichier(fichier.getPath())) {
                // Définition des valeurs des paramètres
                num = Integer.parseInt(l[0]);
                qualification = this.getQualification(l[1]);
                nbHeures = Integer.parseInt(l[2]);
                planning = new HashMap<>();


                // Seulement si le personnel en cours à un plainng d'établi
                if (!l[3].isEmpty()) {
                    // Séparation du champ ENTRE/SORTIE (fichier CSV) afin d'obtenir un couple "(codeElement, qte)"
                    String [] couples = l[3].split("-");

                    // Parcours de la liste des couples
                    for (String s : couples) {
                        // Séparation des couples afin d'obtenir deux chaines séparées le "codeElement" et la "qte"
                        String [] chaine = s.split(",");

                        // Ajout à la HasMap
                        planning.put(this.getChaineByCode(Controleur.listeChaine, chaine[0].substring(1)), Integer.parseInt(chaine[1].substring(0,chaine[1].length()-1)));
                    }
                }

                Personnel p = new Personnel(num, qualification, nbHeures, planning);
                listePersonnel.add(p);
            }
        }

        return listePersonnel;
    }


    /**
     * Exporte les chaines de production de l'usine saisie dans l'application dans le format CSV
     * @param repertoire lieu d'enregistrement
     * @throws IOException Erreur lors de l'export des chaines de production
     */
    @Override
    public void exportChaine(File repertoire) throws IOException {
        StringBuilder sr = new StringBuilder();
        // En-tête du fichier CSV exporté
        sr.append("Code;Nom;Entree (code,qte);Sortie (code,qte);Niveau;Temps;Personnels non qualifies;Personnels qualifies\n");

        // Parcours des chaines de prod (ajout des lignes aux fichiers)
        for (ChaineProd c : Controleur.listeChaine) {
            sr.append(c.getCode()).append(";").append(c.getNom()).append(";");

            // Parcours de la hashmap des éléments
            int cptEntree = 1;
            for (Map.Entry<Element, Integer> e : c.getListeEntree().entrySet()) {
                sr.append("(").append(e.getKey().getCode()).append(",").append(e.getValue()).append(")");
                if (cptEntree != c.getListeEntree().size()) {
                    sr.append("-");
                }
                cptEntree++;
            }

            sr.append(";");

            // Parcours de la hasmap des produits
            int cptSortie = 1;
            for (Map.Entry<Produit, Integer> e : c.getListeSortie().entrySet()) {
                sr.append("(").append(e.getKey().getCode()).append(",").append(e.getValue()).append(")");
                if (cptSortie != c.getListeSortie().size()) {
                    sr.append("-");
                }
                cptSortie++;
            }

            sr.append(";");

            sr.append(c.getNiveau()).append(";").append(c.getTemps()).append(";").append(c.getpNonQualifie()).append(";").append(c.getpQualifie()).append("\n");
        }

        // Ecrire dans le fichier
        FileWriter f = new FileWriter(repertoire.getPath() + "\\chaines.csv");
        f.write(sr.toString());
        f.close();
    }


    /**
     * Exporte le stock (les éléments) de l'usine saisie dans l'application dans le format CSV
     * @param repertoire lieu d'enregistrement
     * @throws IOException Erreur lors de l'export du stock
     */
    @Override
    public void exportStock(File repertoire) throws IOException {
        StringBuilder sr = new StringBuilder();

        // En-tête du fichier CSV exporté
        sr.append("Code;Nom;Quantite;Unite;PrixAchat;PrixVente\n");

        // Formatage de chaque ligne de chaque élément (de type element ou produit)
        for (Element e : Controleur.listeStock) {
            if (e instanceof Produit) {
                if (e.getpAchat() == 0 && ((Produit) e).getpVente() == 0) {
                    sr.append(e.getCode()).append(";").append(e.getNom()).append(";").append(e.getQte()).append(";").append(e.getUnite().toString()).append(";").append("NA;NA\n");
                }
                else {
                    if (e.getpAchat() == 0) {
                        sr.append(e.getCode()).append(";").append(e.getNom()).append(";").append(e.getQte()).append(";").append(e.getUnite().toString()).append(";").append("NA;").append(((Produit) e).getpVente()).append("\n");
                    }

                    if (((Produit) e).getpVente() == 0) {
                        sr.append(e.getCode()).append(";").append(e.getNom()).append(";").append(e.getQte()).append(";").append(e.getUnite().toString()).append(";").append(e.getpAchat()).append(";NA\n");
                    }
                }
            }
            else {
                sr.append(e.getCode()).append(";").append(e.getNom()).append(";").append(e.getQte()).append(";").append(e.getUnite().toString()).append(";").append(e.getpAchat()).append(";NA\n");
            }
        }

        // Ecrire dans le fichier
        FileWriter f = new FileWriter(repertoire.getPath() + "\\elements.csv");
        f.write(sr.toString());
        f.close();
    }


    /**
     * Exporte le personnel de l'usine saisie dans l'application dans le format CSV
     * @param repertoire lieu d'enregistrement
     * @throws IOException Erreur lors de l'export du personnel
     */
    @Override
    public void exportPersonnel(File repertoire) throws IOException {
        StringBuilder sr = new StringBuilder();

        // En-tête du fichier CSV exporté
        sr.append("Numero;Qualification;NombreHeure;Planning(codeChaine,Nbheure)\n");

        // Formatage de chaque ligne de chaque élément (de type element ou produit)
        for (Personnel p : Controleur.listePersonnel) {
            sr.append(p.getNum()).append(";").append(p.getQualif()).append(";").append(p.getNbHeuresMax()).append(";");

            if (p.getPlanning().size() > 0) {
                // Parcours de la hashmap des éléments
                int cpt = 1;
                for (Map.Entry<ChaineProd, Integer> e : p.getPlanning().entrySet()) {
                    sr.append("(").append(e.getKey().getCode()).append(",").append(e.getValue()).append(")");
                    if (cpt != p.getPlanning().size()) {
                        sr.append("-");
                    }
                    cpt++;
                }
            }

            // Fin de la ligne
            sr.append("\n");
        }

        // Ecrire dans le fichier
        FileWriter f = new FileWriter(repertoire.getPath() + "\\personnel.csv");
        f.write(sr.toString());
        f.close();
    }



    // ********************************************************
    // ******************* METHODES PRIVEES *******************
    // ********************************************************

    /**
     * Permet d'obtenir l'unité d'un élément contenu dans le fichier CSV
     * @param u chaine qui contient l'unite de l'élément
     * @return l'unité correspondant à l'enum Unite
     */
    private Unite getUnite(String u) {
        switch (u.toLowerCase()) {
            case "kg":
                return Unite.kg;
            case "litre" :
                return Unite.litre;
            case "unite" :
                return Unite.unité;
            case "carton" :
                return Unite.carton;
            default:
                return Unite.aucune;
        }
    }


    /**
     * Permet d'obtenir la qualification d'un personnel contenu dans le fichier CSV
     * @param q qualification du personnel analysé
     * @return la qualification correspondant à l'enum Qualification
     */
    private Qualification getQualification(String q) {
        if ("qualifie".equals(q.toLowerCase())) {
            return Qualification.QUALIFIE;
        }
        return Qualification.NON_QUALIFIE;
    }


    /**
     * Retourne l'ensemble des lignes d'un fichier CSV passé en pramètre
     * @param path Chemin du fichier
     * @return Les lignes contenus dans le fichier
     * @throws Exception Si fichier non trouvé ou fichier impossible à charger
     */
    private List<String[]> getLignesFichier(String path) throws Exception {
        FileReader file = new FileReader(path);

        // Permet de sauter la première ligne du fichier qui contient les noms des attributs
        CSVReader reader = new CSVReader(file, ';', '\'', 1);


        List<String[]> lignes = reader.readAll();
        if (lignes == null) {
            throw new IOException ("Erreur : impossible de charger les lignes du fichier");
        }

        reader.close();

        return lignes;
    }


    /**
     * Retourne un élément existant dans le stock suivant son code
     * @param listeElement Stock de l'usine chargé précédement
     * @param code Code de l'élément recherché
     * @return Objet élément recherché
     */
    private Element getElementByCode(ObservableList<Element> listeElement, String code) {
        // Parcours de la liste
        for (Element e : listeElement) {
            if (e.getCode().toLowerCase().contains(code.toLowerCase())) {
                return e;
            }
        }

        return null;
    }


    /**
     * Retourne une Chaine de production existante suivant son code
     * @param listeChaine Liste des chaines de prduction dans l'usine
     * @param code Code de la chaine recherchée
     * @return Objet chaine de production recherché
     */
    private ChaineProd getChaineByCode(ObservableList<ChaineProd> listeChaine, String code) {
        // Parcours de la liste
        for (ChaineProd c : listeChaine) {
            if (c.getCode().contains(code)) {
                return c;
            }
        }

        return null;
    }


    /**
     * Retourne une HaspMap avec comme clé un Element et en valeur la quantité nécessaire de l'élément
     * @param listeStock Stock de l'usine chargé précédement
     * @param champ Champ du fichier csv comportant une chaine de caractère avec les éléments ed listé avec leur quantité
     * @return HasMap qui correspond aux éléments attendus à l'entré d'une chaine de production
     */
    private HashMap<Element, Integer> getMapChaineInputOutputElement(ObservableList<Element> listeStock, String champ) {
        HashMap<Element, Integer> map = new HashMap<>();

        // Séparation du champ ENTRE/SORTIE (fichier CSV) afin d'obtenir un couple "(codeElement, qte)"
        String [] couples = champ.split("-");

        // Parcours de la liste des couples
        for (String s : couples) {
            // Séparation des couples afin d'obtenir deux chaines séparées le "codeElement" et la "qte"
            String [] chaine = s.split(",");

            // Ajout à la HasMap
            map.put(this.getElementByCode(listeStock, chaine[0].substring(1)), Integer.parseInt(chaine[1].substring(0,chaine[1].length()-1)));
        }

        return map;
    }


    /**
     * Retourne une HaspMap avec comme clé un Element et en valeur la quantité nécessaire de l'élément
     * @param listeStock Stock de l'usine chargé précédement
     * @param champ Champ du fichier csv comportant une chaine de caractère avec les éléments ed listé avec leur quantité
     * @return HasMap qui correspond aux éléments attendus à la sortie d'une chaine de production
     */
    private HashMap<Produit, Integer> getMapChaineInputOutputProduit(ObservableList<Element> listeStock, String champ) {
        HashMap<Produit, Integer> map = new HashMap<>();

        // Séparation du champ ENTRE/SORTIE (fichier CSV) afin d'obtenir un couple "(codeElement, qte)"
        String [] couples = champ.split("-");

        // Parcours de la liste des couples
        for (String s : couples) {
            // Séparation des couples afin d'obtenir deux chaines séparées le "codeElement" et la "qte"
            String [] chaine = s.split(",");

            // Ajout à la HasMap
            map.put((Produit) this.getElementByCode(listeStock, chaine[0].substring(1)), Integer.parseInt(chaine[1].substring(0,chaine[1].length()-1)));
        }

        return map;
    }

}
