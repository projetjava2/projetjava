package com.mycompany.managefactory.technique;

import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.element.Text;
import com.itextpdf.layout.property.TextAlignment;
import com.mycompany.managefactory.controleur.Controleur;
import com.mycompany.managefactory.modele.ChaineProd;
import com.mycompany.managefactory.modele.Personnel;
import com.mycompany.managefactory.modele.Qualification;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Classe qui permet de générer le PDF du résultat
 */
public class PDF {

    // **********************************************************
    // ******************* METHODES PUBLIQUES *******************
    // **********************************************************

    /**
     * Génére un fichier PDF du résultat
     * @param fichier chemin d'enregistrement du fichier
     * @throws FileNotFoundException erreur de création du fichier
     */
    public static void resultatPDF (File fichier, double ca, double tauxCmdSatisfaites) throws Exception {

        // Ecriture du pdf à l'endroit sélectionné
        PdfWriter writer = new PdfWriter(fichier);

        // Initialisation du document
        PdfDocument pdfDoc = new PdfDocument(writer);

        // Création du document
        Document document = new Document(pdfDoc);

        // tools pour l'édition du document
        PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);

        // Titre du document
        enTeteDocument(document, bold);

        // Ajout du CA
        ajoutCA(document, ca);

        // Ajout du détail du personnel
        ajoutPersonnel(document);

        // Ajout du détail du teux des commandes satisfaite
        ajoutTauxCmdSatisfaite(document, tauxCmdSatisfaites);

        // Ajout du détail des chaines
        ajoutDetailChaine(document);

        // Fermeture du document
        document.close();
    }



    // ********************************************************
    // ******************* METHODES PRIVEES *******************
    // ********************************************************

    /**
     * Ajout de l'en-tête du document
     * @param document document en cour
     * @param bold font
     */
    private static void enTeteDocument(Document document, PdfFont bold) {
        // Titre
        Text title = new Text("Résultat Simulation\n").setFont(bold).setFontSize(18);
        Paragraph pTitre = new Paragraph().add(title).setTextAlignment(TextAlignment.CENTER);
        document.add(pTitre);

        // Date de la simulation
        Date dateSimulation = new Date(System.currentTimeMillis());
        Text date = new Text(new SimpleDateFormat("dd-MM-yyyy").format(dateSimulation));
        Paragraph pDate = new Paragraph().add(date).setTextAlignment(TextAlignment.RIGHT);
        document.add(pDate);
    }


    /**
     * Ajout le chiffre d'affaire de l'entreprise au fichier PDF
     * @param document document en cours de création
     */
    private static void ajoutCA(Document document, double ca) {
        String caTexte = "Chiffre d'affaire de l'entreprise : " + ca + " €";
        document.add(newParagraphe(caTexte));
    }


    /**
     * Ajout des informations concernant le personnel
     * @param document document en cours de création
     */
    private static void ajoutPersonnel(Document document) {
        int nbHeureRestantePersoQ = 0;
        int nbHeureRestantePersoNonQ = 0;

        // Calcul du nombre total d'heure restante
        for (Personnel p : Controleur.listePersonnel) {
            if (p.getQualif().equals(Qualification.QUALIFIE)) {
                nbHeureRestantePersoQ += p.nbHeureRestante();
            }
            else {
                nbHeureRestantePersoNonQ += p.nbHeureRestante();
            }
        }

        // Formatage de la chaine
        String detailPerso = "Nombre d'heure restant pour le personnel qualifié : " + nbHeureRestantePersoQ + " heures\n";
        detailPerso += "Nombre d'heure restant pour le personnel non qualifié : " + nbHeureRestantePersoNonQ + " heures";

        // Personnel qualifié
        document.add(newParagraphe(detailPerso));
    }


    /**
     * Ajout du taux de commandes satisfaite pour la simulation
     * @param document document en cours de création
     */
    private static void ajoutTauxCmdSatisfaite(Document document, double tauxCmdSatisfaites) {
        String tauxCmd = "Taux de commandes satisfaites : " + tauxCmdSatisfaites + " %";
        document.add(newParagraphe(tauxCmd));
    }


    /**
     * Ajout du détail de chaque chaine au document
     * @param document document en cours de création
     */
    private static void ajoutDetailChaine(Document document) {
        // Detail chaine production
        String detail = "Détail des chaines de production :";
        document.add(newParagraphe(detail));

        // Création et configuration du tableau
        Table table = new Table(new float[6]).useAllAvailableWidth();
        table.setMarginTop(0);
        table.setMarginBottom(0);

        // Définition du titre de chaque colonne
        table.addCell("Code").setTextAlignment(TextAlignment.CENTER);
        table.addCell("Nom").setTextAlignment(TextAlignment.CENTER);
        table.addCell("Niveau").setTextAlignment(TextAlignment.CENTER);
        table.addCell("Satut").setTextAlignment(TextAlignment.CENTER);
        table.addCell("Indicateur de valeur").setTextAlignment(TextAlignment.CENTER);
        table.addCell("Indicateur de commande").setTextAlignment(TextAlignment.CENTER);

        // Ajout des données au tableau
        for (ChaineProd c : Controleur.listeChaine) {
            table.addCell(c.getCode());
            table.addCell(c.getNom()).setTextAlignment(TextAlignment.JUSTIFIED);
            table.addCell(String.valueOf(c.getNiveau())).setTextAlignment(TextAlignment.CENTER);
            table.addCell(String.valueOf(c.getStatutProperty().get()));
            table.addCell(c.getRentabiliteProperty().get() + " €");
            table.addCell(c.demandeSatisfaite().get() + " %");
        }

        document.add(table);
    }


    /**
     * Créer un Paragraphe pour le texte passé en paramètre
     * @param texteParagraphe document en cours de création
     * @return le paragraphe créé
     */
    private static Paragraph newParagraphe(String texteParagraphe) {
        Text texte = new Text(texteParagraphe);
        return new Paragraph().add(texte).setTextAlignment(TextAlignment.JUSTIFIED);
    }

}
