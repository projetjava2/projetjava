package com.mycompany.managefactory.technique;

import com.mycompany.managefactory.modele.ChaineProd;
import com.mycompany.managefactory.modele.Demande;
import com.mycompany.managefactory.modele.Element;
import com.mycompany.managefactory.modele.Personnel;
import javafx.collections.ObservableList;

import java.io.File;
import java.io.IOException;

/**
 * Interface à implémenter suivant les sources de données
 */
public interface DonneesStrategy {
    /**
     * Retourne les Elements chargés depuis la source de données
     * @return Retourne une collection observable d'objet Elements
     * @throws Exception Erreur lors du chargement des Elements
     */
    ObservableList<Element> loadElement() throws Exception;


    /**
     * Retourne les Chaines de Production chargées depuis la source de données
     * @return Retourne une collection observable d'objet ChaineProd
     * @throws Exception Erreur lors du chargement des Chaines
     */
    ObservableList<ChaineProd> loadChaine() throws Exception;


    /**
     * Retourne les Demandes chargées depuis la source de données
     * @return Retourne une collection observable d'objet Demande
     * @throws Exception Erreur lors du chargement des demandes
     */
    ObservableList<Demande> loadDemande() throws Exception;


    /**
     * Retourne le personnel de l'usine chargé depuis la source de données
     * @return Retourne une collection observable d'objet Personnel
     * @throws Exception Erreur lors du chargement du personnel
     */
    ObservableList<Personnel> loadPersonnel() throws Exception;


    /**
     * Exporte les chaines de production de l'usine saisie dans l'application dans le format souhaité
     * @param repertoire lieu d'enregistrement
     * @throws IOException Erreur lors de l'export des chaines de production
     */
    void exportChaine(File repertoire) throws IOException;


    /**
     * Exporte le stock (les éléments) de l'usine saisie dans l'application dans le format souhaité
     * @param repertoire lieu d'enregistrement
     * @throws IOException Erreur lors de l'export du stock
     */
    void exportStock(File repertoire) throws IOException;


    /**
     * Exporte le personnel de l'usine saisie dans l'application dans le format souhaité
     * @param repertoire lieu d'enregistrement
     * @throws IOException Erreur lors de l'export du personnel
     */
    void exportPersonnel(File repertoire) throws IOException;

}
