package com.mycompany.managefactory.modele;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 * Classe d'un objet Demande de l'usine
 */
public class Demande {

    // *********************************************
    // ***************** ATTRIBUTS *****************
    // *********************************************

    private Element element;
    private IntegerProperty qteDem;



    // *****************************************************
    // ******************* CONSTRUCTEUR *******************
    // *****************************************************

    /**
     * Constructeur d'un objet Demande
     * @param element : correspond à l'élément demandé
     * @param qteDem : quantité demandée
     */
    public Demande(Element element, int qteDem) {
        this.element = element;
        this.qteDem = new SimpleIntegerProperty(qteDem);
    }



    // **********************************************
    // ******************* GETTER *******************
    // **********************************************
    
    /**
     * Retourne l'Elément qui correspond à la demande
     */
    public Element getElement() {
        return this.element;
    }


    /**
    * Retourne la quantité demandé de l'Element pour une demande
    */
    public int getQteDem() {
        return this.qteDem.get();
    }


    /**
     * Retourne la PROPERTY de la quantité demandé de l'Element pour une demande
    */
    public IntegerProperty getQteDemProperty() {
    	return this.qteDem;
    }

}
