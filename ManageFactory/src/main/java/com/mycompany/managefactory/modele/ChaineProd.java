package com.mycompany.managefactory.modele;

import com.mycompany.managefactory.controleur.Controleur;
import javafx.beans.property.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Classe d'un objet Chaine de Production de l'usine
 */
public class ChaineProd {

    // *********************************************
    // ***************** ATTRIBUTS *****************
    // *********************************************

    private StringProperty code;
    private StringProperty nom;
    private IntegerProperty niveau;
    private BooleanProperty statut;
    private IntegerProperty temps;
    private IntegerProperty pQualifie;
    private IntegerProperty pNonQualifie;
    private DoubleProperty rentabilite;
    private HashMap<Element, Integer> listeEntree;
    private HashMap<Produit, Integer> listeSortie;
    private BooleanProperty statutAffectationPersonnel;
    private int oldNiveau;
    private int hFinCycle;
    // Liste des chaines dont la production est nécessaire à la chaine courante (prédécesseurs)
    private ArrayList<ChaineProd> listeChainesCritiques;
    // Liste des chaines qui dépendent de la production de la chaine courante (successeurs)
    private ArrayList<ChaineProd> listeChaineDependantes;



    // ****************************************************
    // ******************* CONSTRUCTEUR *******************
    // ****************************************************

    /**
     * Constructeur d'une chaine de production
     * @param code Code de la chaine de prodcution
     * @param nom Désignation de la chaine de production (ce qu'elle produit)
     * @param niveau Niveau de production de la chaine de production
     * @param temps Temps de production de la chaine de production
     * @param pQualifie Nombre de personnes qualifiées nécessaire au focntionnement de la chaine de production
     * @param pNonQualifie Nombre de personnes non qualifiées nécessaire au focntionnement de la chaine de production
     * @param listeEntree Eléments nécessaires à la production des produits en sortie de la chaine de production
     * @param listeSortie Produits sorties à la suite de la production de la chaine
     */
    public ChaineProd(String code, String nom, int niveau, int temps, int pQualifie, int pNonQualifie, HashMap<Element, Integer> listeEntree, HashMap<Produit, Integer> listeSortie) {
        this.code = new SimpleStringProperty(code);
        this.nom = new SimpleStringProperty(nom);
        this.niveau = new SimpleIntegerProperty(niveau);
        this.temps = new SimpleIntegerProperty(temps);
        this.pQualifie = new SimpleIntegerProperty(pQualifie);
        this.pNonQualifie = new SimpleIntegerProperty(pNonQualifie);
        this.listeEntree = listeEntree;
        this.listeSortie = listeSortie;
        this.statutAffectationPersonnel = new SimpleBooleanProperty(false);
        this.prodPossible();
        this.rentabilite = new SimpleDoubleProperty(this.rentabilite());
        this.oldNiveau = this.niveau.get();
        this.hFinCycle = 0;

        // ces listes ne peuvent pas être rempli à l'initialisation d'un objet chaine car toutes les chaines ne sont pas forcément importées
        this.listeChainesCritiques = new ArrayList<>();
        this.listeChaineDependantes = new ArrayList<>();
    }



    // *******************************************************
    // ******************* GETTER / SETTER *******************
    // *******************************************************

    /**
     * Retourne la chaîne de caractere qui correspond au code d'une chaine de production
     * @return Code de la chaine de la chaine de production
     */
    public String getCode() {
        return code.get();
    }


    /**
     * Retourne la chaîne de caractere qui correspond au nom chaine de production
     * @return Nom de la chaine de production
     */
    public String getNom() {
        return nom.get();
    }


    /**
     * Retourne l'entier qui correspond au niveau d'une chaine de production
     * @return Niveau de la chaine de production
     */
    public int getNiveau() {
        return niveau.get();
    }


    /**
     * Retourne un booléen qui correspond au statut d'une chaine de production
     * @return Statut de la chaine de production (true : MARCHE / false : ARRET)
     */
    public boolean isStatut() {
        return statut.get();
    }


    /**
     * Retourne l'entier qui correspond au temps de production d'une chaine de production pour un cycle
     * @return Temps de production d'un cycle
     */
    public int getTemps() {
        return temps.get();
    }


    /**
     * Retourne l'entier qui correspond au nombre de personne qualifié nécessaire pour une chaine de production pour un cycle
     * @return Nombre de personne qualifié
     */
    public int getpQualifie() {
        return pQualifie.get();
    }


    /**
     * Retourne l'entier qui correspond au nombre de personne NON qualifié nécessaire pour une chaine de production pour un cycle
     * @return Nombre de personne NON qualifié
     */
    public int getpNonQualifie() {
        return pNonQualifie.get();
    }


    /**
     * Retourne la HashMap qui correspond à la liste des Elements nécessaires à un cycle de production (clé : Element, valeur : Nombre d'Element nécessaire à la production)
     * @return HashMap des Element avec la quantité nécessaire à la production de chaque Element
     */
    public HashMap<Element, Integer> getListeEntree() {
        return listeEntree;
    }


    /**
     * Retourne la HashMap qui correspond à la liste des Element (de type Produit) produit à la suite d'un cycle de production (clé : Produit, valeur : Nombre de Produit conçu à la fin du cycle)
     * @return HashMap des Produits avec la quantité produite à la suite d'un cycle de production
     */
    public HashMap<Produit, Integer> getListeSortie() {
        return listeSortie;
    }


    /**
     * Retourne un booléen qui correspond au statut de l'affectation du personnel d'une chaine de production
     * @return Statut de la chaine de production (true : Affectation effectuée / false : Affectation non effectuée)
     */
    public boolean getStatutAffectationPersonnel() {
        return this.statutAffectationPersonnel.get();
    }


    /**
     * Retourne la PROPERTY chaîne de caractere qui correspond au code d'une chaine de production
     * @return PROPERTY Code de la chaine de la chaine de production
     */
    public StringProperty getCodeProperty() {
        return code;
    }


    /**
     * Retourne la PROPERTY chaîne de caractere qui correspond au nom chaine de production
     * @return PROPERTY Nom de la chaine de production
     */
    public StringProperty getNomProperty() {
        return nom;
    }


    /**
     * Retourne la PROPERTY de l'entier qui correspond au niveau d'une chaine de production
     * @return PROPERTY Niveau de la chaine de production
     */
    public IntegerProperty getNiveauProperty() {
        return niveau;
    }


    /**
     * Retourne la PROPERTY chaîne de caractere qui correspond au statut d'une chaine de production
     * @return PROPERTY Statut de la chaine de production (MARCHE / ARRET)
     */
    public StringProperty getStatutProperty() {
        StringProperty statut;
        if (this.statut.get()) {
            statut = new SimpleStringProperty("MARCHE");
        } else {
            statut = new SimpleStringProperty("ARRET");
        }
        return statut;
    }


    /**
     * Retourne la PROPERTY du double qui correspond au niveau de rentabilité de la chaine de production
     * @return PROPERTY Rentabilité de la chaine de production
     */
    public DoubleProperty getRentabiliteProperty() {
        return this.rentabilite;
    }


    /**
     * Retourne la PROPERTY chaîne de caractere qui correspond au statut de l'affectation du personnel pour une chaine de production
     * @return PROPERTY Statut de l'affectation du personnel à la chaine de production (AFFECTE / NON AFFECTE)
     */
    public StringProperty getStatutAffectationPersonnelProperty() {
        StringProperty statut;
        if (this.statutAffectationPersonnel.get()) {
            statut = new SimpleStringProperty("AFFECTÉ");
        } else {
            statut = new SimpleStringProperty("NON AFFECTÉ");
        }
        return statut;
    }


    /**
     * Retourne la PROPERTY de l'entier qui correspond au nombre de personne qualifié nécessaire pour une chaine de production pour un cycle
     * @return PROPERTY Nombre de personne qualifié
     */
    public IntegerProperty getPQualifieProperty() {
        return this.pQualifie;
    }


    /**
     * Retourne la PROPERTY de l'entier qui correspond au nombre de personne NON qualifié nécessaire pour une chaine de production pour un cycle
     * @return PROPERTY Nombre de personne NON qualifié
     */
    public IntegerProperty getPNonQualifieProperty() {
        return this.pNonQualifie;
    }


    /**
     * Permet de modifier le niveau d'activation de la chaîne de production et met à jour son niveau de rentabilité et le stock (entré et sortie)
     * Détermine également si la chaine est en attente de la prodcution d'une autre chaine auquel cas elle sera placé en attente
     * @param niveau nouvelle valeur du niveau d'activation de la châine
     */
    public void setNiveau(int niveau) {
        int differenceNiveau;
        if (niveau >= 0) {
            // Si le niveau est supérieur au niveau actuel
            if (niveau > this.niveau.get()) {
                // Calcul la différence de niveau
                differenceNiveau = niveau - this.niveau.get();
                // Calcul la différence d'heure travaillé entre l'ancin niveau et le nouveau
                int ancienNbHeureTravaille = this.niveau.get() * this.temps.get();

                // Set le nouveau niveau afin de définir si la production est possible
                this.oldNiveau = this.niveau.get();
                this.niveau = new SimpleIntegerProperty(niveau);
                this.prodPossible();
                this.statutAffectationPersonnel = new SimpleBooleanProperty(this.affecterPersonnel());

                // Si la production est possible ET que l'affectation du personnel à réussi
                // alors on définit la rentabilité et on gère les flux de marchandise nécessaire
                if (this.statut.get() && this.statutAffectationPersonnel.get()) {
                    this.rentabilite = new SimpleDoubleProperty(this.rentabilite());
                    this.augmentationNiveauAjoutStock(differenceNiveau);
                    this.augmentationNiveauReduireStock(differenceNiveau);

                    // Ajoute la chaine aux heures ou elle sera active
                    this.ajouterChainePlanning(ancienNbHeureTravaille);

                    // Ajoute au personnel qui travail sur la chaine l'heure de leur prochaine disponibilité
                    this.heureFinCycle();

                    // Vérifiaction si des chaines en attente peuvent démarer
                    this.activationChaineEnAttente();
                }
                else {
                    // Supprimer l'affectation du presonnel
                    this.statutAffectationPersonnel = new SimpleBooleanProperty(false);
                    this.heureFinCycle();
                    this.supprimerAffectation();

                    // La chaine est mise en attente
                    Controleur.listeChaineEnAttente.add(this);
                }
            }
            // Diminution du niveau de production OU Le niveau passé en paramètre et égale au niveau d'activation de la chaine -> tentaive de réactivation
            else {
                // Cas 1 : diminution du niveau d'activation de la chaine
                if (niveau < this.niveau.get()) {
                    // Calcul la différence de niveau
                    differenceNiveau = this.niveau.get() - niveau;
                    // On restock les éléments en fonction de la différence de niveau
                    this.diminutionNiveauAjoutStock(differenceNiveau);
                    this.diminutionNiveauReduireStock(differenceNiveau);

                    this.niveau = new SimpleIntegerProperty(niveau);
                    this.prodPossible();


                    // Retrait de la chaine des heures du planning en fonction de la baisse du niveau d'activation
                    int i = Controleur.planningChaine.size();
                    int cpt = 0;
                    // On parcours le planning des chaines en partant de la fin et on retire le nombre d'heure nécessaire en fonction du nouveau niveau d'action
                    while (i > 0 && cpt < differenceNiveau) {
                        // Si la chaine est présente dans le cycle en cours de parcours
                        if (Controleur.planningChaine.get(i).contains(this)) {
                            Controleur.planningChaine.get(i).remove(this);
                            cpt++;
                        }
                        i--;
                    }
                    // Change l'heure de fin de cycle de la chaine de production
                    this.heureFinCycle();

                    // Suppression du personnel affecté avant réaffectation en fonction du nouveau niveau d'activation
                    //this.supprimerAffectation();
                    this.statutAffectationPersonnel = new SimpleBooleanProperty(this.affecterPersonnel());

                    // Si la production est possible alors on définit la rentabilité et on gère les flux de marchandise nécessaire
                    if (this.statut.get() && this.statutAffectationPersonnel.get()) {
                        this.rentabilite = new SimpleDoubleProperty(this.rentabilite());
                        this.heureFinCycle();
                    }

                    // Vérification que la diminution de son niveau d'activation n'a pas d'impact sur les chaines qui dépendent de sa production
                    if (this.listeChaineDependantes.size() > 0) {
                        for (ChaineProd c : this.listeChaineDependantes) {
                            // Si la chaine fonctionne
                            if (c.isStatut()) {
                                // Vérification si la prodcution de la chaine de production est toujours possible en fonction du stock des element qu'elle attend
                                c.prodPossible();

                                // Si après vérification le statut de la chaine a changé alors
                                // on supprime l'affectation du personnel et on la met en attente
                                if (!c.isStatut()) {
                                    c.supprimerChainePlanning();
                                    c.heureFinCycle();
                                    c.supprimerAffectation();
                                    Controleur.listeChaineEnAttente.add(c);
                                }
                            }
                        }
                    }
                    // Si aucun impact en cascade alors
                    else {
                        this.rentabilite = new SimpleDoubleProperty(0);
                        // Supprimer l'affectation du presonnel
                        this.statutAffectationPersonnel = new SimpleBooleanProperty(false);
                        this.heureFinCycle();
                        this.supprimerAffectation();

                        // La chaine est mise en attente
                        if (!Controleur.listeChaineEnAttente.contains(this)) {
                            if (this.listeChainesCritiques.size() != 0) {
                                Controleur.listeChaineEnAttente.add(this);
                            }
                        }
                    }
                }

                // Cas 2 : tentative de réactivation de la chaine
                if (!this.statut.get() && niveau == this.getNiveau()) {
                    // Set le nouveau niveau afin de définir si la production est possible
                    this.niveau = new SimpleIntegerProperty(niveau);
                    this.prodPossible();
                    this.statutAffectationPersonnel = new SimpleBooleanProperty(this.affecterPersonnel());

                    // Si la production est possible ET que l'affectation du personnel à réussi ET que la chaine n'a pas de dépendance
                    // alors on définit la rentabilité et on gère les flux de marchandise nécessaire
                    if (this.statut.get() && this.statutAffectationPersonnel.get()) {
                        this.rentabilite = new SimpleDoubleProperty(this.rentabilite());
                        this.augmentationNiveauAjoutStock(niveau);
                        this.augmentationNiveauReduireStock(niveau);

                        // Vérifiaction si des chaines en attente peuvent démarer
                        this.activationChaineEnAttente();

                        // Ajoute la chaine aux heures ou elle sera active
                        this.ajouterChainePlanning(0);

                        // MAJ de son heure de fin de cycle et de celle du personnel qui travail sur la chaine
                        this.heureFinCycle();
                    }
                    else {
                        // Supprimer l'affectation du presonnel
                        this.statutAffectationPersonnel = new SimpleBooleanProperty(false);
                        this.heureFinCycle();
                        this.supprimerAffectation();

                        // La chaine est mise en attente
                        if (!Controleur.listeChaineEnAttente.contains(this)) {
                            if (this.listeChainesCritiques.size() != 0) {
                                Controleur.listeChaineEnAttente.add(this);
                            }
                        }
                    }
                }
            }

            // Si le niveau de la chaine est de 0 alors elle peut être retirée de la liste d'attente ou retiré du planing
            if (this.niveau.get() == 0) {
                if (Controleur.listeChaineEnAttente.contains(this)) {
                    Controleur.listeChaineEnAttente.remove(this);
                }
                for (Integer i : Controleur.planningChaine.keySet()) {
                    if (Controleur.planningChaine.get(i).contains(this)) {
                        Controleur.planningChaine.get(i).remove(this);
                    }
                }
            }

            // Si la chaine n'a aucune raison de se retrouver en attente alors on la supprime de la liste d'attente
            if (Controleur.listeChaineEnAttente.contains(this)) {
                if (this.chainesBloquantes().size() == 0 && this.statut.get() && this.statutAffectationPersonnel.get()) {
                    Controleur.listeChaineEnAttente.remove(this);
                    this.ajouterChainePlanning(0);
                }
            }

        }
        else {
            throw new ArithmeticException("Le niveau d'activation d'une chaîne de production ne doit pas être négative");
        }
    }


    /**
     * Permet de mettre à jour le statut de la chaine
     * @param statut Nouveau statut de la chaine
     */
    public void setStatut(boolean statut) {
        this.statut = new SimpleBooleanProperty(statut);
    }



    // **********************************************************
    // ******************* METHODES PUBLIQUES *******************
    // **********************************************************

    /**
     * Méthode qui permet d'obtenir le montant nécessaire à la production. Ce montant prend en compte les éléments en entrée, leur prix d'achat et leur quantité ansi que le niveau d'activation de la chaîne.
     * @return Montant de production nécessaire
     */
    public double montantProd() {
        double montant = 0;
        for (Map.Entry<Element, Integer> e : this.listeEntree.entrySet()) { // Pour chaque couple de la map
            montant += e.getKey().getpAchat() * e.getValue(); // On ajoute le prix d'achat de l'élément fois la quantité nécessaire
        }
        // Une fois le montant total calculé, on le multiplie par le niveau d'activation de la chaîne :
        montant *= this.niveau.get();
        return montant;
    }


    /**
     * Permet d'obetenir le montant de vente (potentielle) créé grâce à cette chaine de production
     * @return montant de vente créé
     */
    public double montantVente() {
        double montant = 0;
        for (Map.Entry<Produit, Integer> e : this.listeSortie.entrySet()) { // Pour chaque couple de le map

            montant += e.getKey().getpVente() * e.getValue(); // On ajoute la quantité produite fois le prix de vente du produit
        }

        montant *= this.niveau.getValue(); // la production est proportionnelle au niveau d'activation de la chaîne de production
        return montant;
    }


    /**
     * Détermine si le stock nécessaire à la production de la chaine est suffisant en fonction de son niveau d'activiation, met à jour la valeur du statut de la chaine
     */
    public void prodPossible() {
        // indicateur sur la production d'une chaine
        boolean prod = true;

        // Pour qu'une production soit possible il faut que sont niveau d'activation soit supérieur à 0
        if (this.niveau.get() > 0) {
            for (Map.Entry<Element, Integer> e : this.listeEntree.entrySet()) {
                // Si la quantité nécessaire fois le niveau d'activation est supérieure à la quantité en stock de l'élément correspondant
                if (e.getValue() * this.niveau.get() > e.getKey().getQte() && prod) {
                    prod = false;
                }
            }
        } else {
            prod = false;
        }

        this.statut = new SimpleBooleanProperty(prod);
    }


    /**
     * Permet d'évaluer la rentabilité de la chaine de production en fonction du montant nécessaire et du montant de richesse créé.
     * @return correspond à l'indicateur de valeur de la chaîne en question
     */
    public double rentabilite() {
        if (this.niveau.get() != 0) {
            return this.montantVente() - this.montantProd();
        } else {
            return 0;
        }
    }


    /**
     * Permet d'obtenir l'indicateur de commande, qui correspond au pourcentage de demandes satisfaites
     * @return indicateur de commande (en %)
     */
    public DoubleProperty demandeSatisfaite() {
        DoubleProperty idCmd;
        if (this.statut.get()) {
            int qteProduite = 0;
            int nbDemande = 0;
            double iCommande = 0.0;

            // l'indicateur est alors égal à : qteProduite*100/nbDemande
            // On regarde ce que la chaine produit (parcour de la listeSortie)
            for (Map.Entry<Produit, Integer> e : this.listeSortie.entrySet()) {
                // Pour chaque élément produit, on en détermine sa quantité grâce au niveau d'activation de la chaine de prod
                qteProduite += e.getValue() * this.niveau.get();

                for (Demande d : Controleur.listeDemande) {
                    if (e.getKey().equals(d.getElement())) {
                        nbDemande += d.getQteDem(); // On détermine le nombre de demandes pour l'élément concerné
                    }
                }
                if (nbDemande != 0) {
                    iCommande = qteProduite * 100 / (float) nbDemande; // Si deux éléments en sortie, indicateur global aux deux éléments et non propres à chaque éléments
                } else {
                    iCommande = 0;
                }
            }
            if (iCommande > 100) {
                iCommande = 100;
            }
            idCmd = new SimpleDoubleProperty(Math.round(iCommande * 100.0) / 100.0);
        } else {
            idCmd = new SimpleDoubleProperty(0);
        }

        return idCmd;
    }


    /**
     * Initialise la liste des chaines qui dépendent de la production de la chaine courante (successeurs)
     */
    public void chainesDependances() {
        // Parcours des éléments qui sont des produits
        for (Element e : this.listeSortie.keySet()) {
            // Parmis les chaines de production lesquelles produisent l'élément nécessaire
            for (ChaineProd c : Controleur.listeChaine) {
                if (c.getListeEntree().containsKey(e)) {
                    this.listeChaineDependantes.add(c);
                }
            }
        }
    }


    /**
     * Initialise la liste des chaines dont la production est nécessaire à la chaine courante (prédécesseurs)
     */
    public void chainesCritiques() {
        // Pour chaque élément attendu en entrée de la chaine de production courrante
        for (Element e : this.listeEntree.keySet()) {
            // On parcours les chaines de production de l'usine
            for (ChaineProd c : Controleur.listeChaine) {
                if (c.getListeSortie().containsKey(e)) {
                    this.listeChainesCritiques.add(c);
                }
            }
        }
    }


    /**
     * Retourne les chaines bloquantes à l'activation de la production
     */
    public ArrayList<ChaineProd> chainesBloquantes() {
        ArrayList<ChaineProd> listeChainesBloquantes = new ArrayList<>();

        // Parcours les chaines dont la chaine courante à des dépendances
        for (ChaineProd c : this.listeChainesCritiques) {
            // Vérifie si la chaine dont elle dépend bloque sa production
            for (Element e : c.getListeSortie().keySet()) {
                if (this.listeEntree.containsKey(e)) {
                    // Compare la quantité en stock et la quantité nécessaire à la production
                    if (this.niveau.get() * this.listeEntree.get(e) > e.getQte()) {
                        // Alors on l'ajoute à la liste des chaines bloquantes
                        listeChainesBloquantes.add(c);
                    }
                }
            }
        }

        return listeChainesBloquantes;
    }



    // ********************************************************
    // ******************* METHODES PRIVEES *******************
    // ********************************************************

    /**
     * Augmente le stock des éléments en sortie de chaine si le niveau de production de la chaine a augmenté lors de sa mise à jour (setNiveau)
     * @param niveau niveau de la chaine
     */
    private void augmentationNiveauAjoutStock(int niveau) {
        for (Element eProduit : this.listeSortie.keySet()) {
            for (Element eMaj : Controleur.listeStock) {
                if (eProduit.getCode().equals(eMaj.getCode())) {
                    eMaj.setQte(eMaj.getQte() + (this.listeSortie.get(eProduit) * niveau));
                }
            }
        }
    }


    /**
     * Diminue le stock des éléments en sortie de chaine si le niveau de production de la chaine a diminué lors de sa mise à jour (setNiveau)
     * @param niveau niveau de la chaine
     */
    private void diminutionNiveauAjoutStock(int niveau) {
        for (Element eProduit : this.listeSortie.keySet()) {
            for (Element eMaj : Controleur.listeStock) {
                if (eProduit.getCode().equals(eMaj.getCode())) {
                    eMaj.setQte(eMaj.getQte() - (this.listeSortie.get(eProduit) * niveau));
                }
            }
        }
    }


    /**
     * Diminue le stock des éléments en sortie de chaine si le niveau de production de la chaine a augmenté lors de sa mise à jour (setNiveau)
     * @param niveau niveau de la chaine
     */
    private void augmentationNiveauReduireStock(int niveau) {
        for (Element eProduit : this.listeEntree.keySet()) {
            for (Element eMaj : Controleur.listeStock) {
                if (eProduit.getCode().equals(eMaj.getCode())) {
                    eMaj.setQte(eMaj.getQte() - (this.listeEntree.get(eProduit) * niveau));
                }
            }
        }
    }


    /**
     * Augmente le stock des éléments en entrée de chaine si le niveau de production de la chaine a diminué lors de sa mise à jour (setNiveau) restockage des produits non utilisés
     * @param niveau niveau de la chaine
     */
    private void diminutionNiveauReduireStock(int niveau) {
        for (Element eProduit : this.listeEntree.keySet()) {
            for (Element eMaj : Controleur.listeStock) {
                if (eProduit.getCode().equals(eMaj.getCode())) {
                    eMaj.setQte(eMaj.getQte() + (this.listeEntree.get(eProduit) * niveau));
                }
            }
        }
    }


    /**
     * Peremet d'affecter le personnel necessaire à la chaine de production (en fonction de son niveau d'activation)
     * @return booléen indiquant si oui ou non l'affectation a bien eu lieu
     */
    private boolean affecterPersonnel() {
        boolean affectation = false;

        // Détermination du nombre de personnes nécessaires
        int persQNecessaire = this.pQualifie.get();
        int persNQNecessaire = this.pNonQualifie.get();
        // Temps de fonctionnement de la chaîne
        int tempsFonct = this.niveau.get() * this.temps.get();

        // Parcours de l'ensemble des employés de l'usine
        for (Personnel pers : Controleur.listePersonnel) {

            int nbHTravail = pers.nbHeureEffectuees();
            int nbHMax = pers.getNbHeuresMax();

            // Employé dispo à partir du dernier cycle de la chaine
            if (this.hFinCycle >= pers.getProchainePrisePoste() ) {

                // L'employé en question peut encore travailler
                if (nbHTravail < nbHMax) {

                    // L'employé peut travailler sur la totalité de la durée de fonctionnement de la production
                    if (nbHMax - nbHTravail >= tempsFonct) {

                        if (pers.getQualif() == Qualification.QUALIFIE && persQNecessaire > 0) {
                            // On ajoute la chaine de prod et le nombre d'heure de fonctionnement de la chaine à son planning :
                            pers.addChainePlanning(this, tempsFonct);
                            // Mise à jour du nombre de personne effectivement nécessaires
                            persQNecessaire--;
                        }
                        else {
                            if (pers.getQualif() == Qualification.NON_QUALIFIE && persNQNecessaire > 0) {
                                // On ajoute la chaine de prod et le nombre d'heure de fonctionnement de la chaine à son planning :
                                pers.addChainePlanning(this, tempsFonct);
                                // Mise à jour du nombre de personne nécessaires
                                persNQNecessaire--;
                            }
                        }
                    }
                    // L'employé peut travailler mais pas sur la totalité du temps de fonctionnement de la chaîne
                    else {

                        int tempsDispo = nbHMax - nbHTravail;

                        // Gestion du personnel qualifié

                        if (pers.getQualif() == Qualification.QUALIFIE && persQNecessaire > 0) {
                            // On ajoute la chaine et son nombre d'heures qu'il peut effectuer au maximum à son planning :
                            pers.addChainePlanning(this, tempsDispo);

                            // On cherche un employé qualifié disponible pour le nombre d'heures restantes.

                            int id = Personnel.persDispo(Controleur.listePersonnel, tempsFonct - tempsDispo, Qualification.QUALIFIE);
                            // On connaît maintenant le numéro de l'employé disponible pour travailler sur le nombre d'heures restantes

                            if (id > 0) { // Il y a bien un employé remplissant ces critères
                                // On lui ajoute la mission (chaine + nombre d'heures) et on peut maintenant seuelement soustraire 1 au nombre de personnel nécessaire
                                Personnel.addChainePlanning(Controleur.listePersonnel, id, this, tempsFonct - tempsDispo);
                                persQNecessaire -= 1;
                            } else {  // Il n'y a pas d'employé disponible pour assurer le nombre d'heures nécessaires pour compléter
                                // Donc pas d'employé dispo pour toute la durée de la chaîne a fortiori
                                affectation = false;
                                return affectation;
                            }
                        }
                        else { // Gestion du personnel non qualifié

                            if (pers.getQualif() == Qualification.NON_QUALIFIE && persNQNecessaire > 0) {
                                // On ajoute la chaine et son nombre d'heures qu'il peut effectuer au maximum à son planning :
                                pers.addChainePlanning(this, tempsDispo);

                                // On cherche un employé non qualifié disponible pour le nombre d'heures restantes.

                                int id = Personnel.persDispo(Controleur.listePersonnel, tempsFonct - tempsDispo, Qualification.NON_QUALIFIE);
                                // On connaît maintenant le numéro de l'employé disponible pour travailler sur le nombre d'heures restantes

                                // Il y a bien un employé remplissant ces critères
                                if (id > 0) {
                                    // On lui ajoute la mission (chaine + nombre d'heures) et on peut maintenant seuelement soustraire 1 au nombre de personnel nécessaire
                                    Personnel.addChainePlanning(Controleur.listePersonnel, id, this, tempsFonct - tempsDispo);
                                    persNQNecessaire -= 1;
                                }
                            }
                        }
                    }
                }
            }
        }

        // A ce stade là :
        // soit tous les personnels ont été affectés;
        // soit il manque des personnels non qualifié qui peuvent être remplacé par des personnel qualifiés


        if (persQNecessaire == 0 && persNQNecessaire == 0) { // Tous les personnels ont été affectés
            affectation = true;
            return affectation;
        }
        else {
            if (persQNecessaire > 0) { // Il manque du personnel qualifié
                affectation = false;
                return affectation;
            }
            else { // S'il manque uniquement du personnel non qualifié, on va chercher du personnel qualifié dispo pour combler le manque
                int i = 0;
                boolean termine = false;

                while (i < persNQNecessaire && termine == false) {
                    // Recherche de personnel QUALIFIE pour la chaîne de production
                    int id = Personnel.persDispo(Controleur.listePersonnel, tempsFonct, Qualification.QUALIFIE);

                    if (id == 0) { // On ne trouve pas de personnel qualifié disponible pour compléter
                        affectation = false;
                        termine = true;
                    }
                    else { // On a bien un employé pouvant compléter l'équipe pour la chaîne de production
                        Personnel.getPersonne(id, Controleur.listePersonnel).addChainePlanning(this, tempsFonct);
                        i++;
                        // Plus besoin de décrémenter le nombre de personnes nécessaires car sinon boucle infinie
                    }
                }
            }
        }

        return affectation;
    }


    /**
     * Supprime les affections du personnel sur cette chaine
     */
    private void supprimerAffectation() {
        for (Personnel p : Controleur.listePersonnel) {
            p.getPlanning().remove(this);
        }
        this.statutAffectationPersonnel = new SimpleBooleanProperty(false);
    }


    /**
     * Active les chaines en attente qui sont dépendantes de la chaine en cours (this)
     */
    private void activationChaineEnAttente() {
        // Si il y a des chaines de production en attente
        if (Controleur.listeChaineEnAttente.size() > 0) {
            // Parcours les chaines de production placée en attente
            for (ChaineProd c : Controleur.listeChaineEnAttente) {
                // Si la chaine de production en attente et qu'elle dépendant de la chaine (this)
                // Alors on reessaie de l'activer avec le niveau d'activation qui lui a été précédement affecté par l'utilisateur
                if (this.listeChaineDependantes.contains(c)) {
                    c.setNiveau(c.getNiveau());
                    // Si la chaine a été activé suite à cette tentative on la retire de la liste d'attente
                    if (c.isStatut()) {
                        Controleur.listeChaineEnAttente.remove(c);
                        c.ajouterChainePlanning(0);
                    }
                }
            }
        }
    }


    /**
     * Supprime la chaine courante du planning
     */
    private void supprimerChainePlanning() {
        for (Integer i : Controleur.planningChaine.keySet()) {
            Controleur.planningChaine.get(i).remove(this);
        }
    }


    /**
     * Ajoute la chaine courante au planning de fonctionnement des chaines de production
     * En veillant à bien respecter les heures de démarage possible en fonction de la production réalisée par les chaines dont elle dépend
     * ou même si cette chaine a déjà été affectée à un cycle de production
     * @param ancienNbHeureTravaille le nombre d'heure de travail déjà effectué par la chaine (peut être égale à 0, si c'est sa première affectation)
     */
    private void ajouterChainePlanning(int ancienNbHeureTravaille) {
        // Correspond à l'heure au plus tôt à laquelle la chaine peut commencer sont cycle de production
        int heureDemarage = 1;

        // Si la chaine n'est pas en attente et qu'elle n'a pas de chiane critique alors elle peut démarer tout de suite
        if (!Controleur.listeChaineEnAttente.contains(this) && this.listeChainesCritiques.size() == 0) {
            // Ajoute la chaine aux heures ou elle sera active sans tenir compte de ses dépendances
            int nbHeureDeTravaille = this.temps.get() * this.niveau.get();
            for (int i = ancienNbHeureTravaille + 1; i <= nbHeureDeTravaille; i++) {
                Controleur.planningChaine.get(i).add(this);
            }
        }

        // Si la chaine n'est pas en attente et qu'elle à des chaines critiques alors elle peut être ajoutée au planning seulement lorsque le stock des éléments
        // nécessaires au démarage de son cycle sera suffisant
        if (!Controleur.listeChaineEnAttente.contains(this) && this.listeChainesCritiques.size() > 0) {
            // Indicateur si la chaine est déjà présente dans le planning
            boolean chainePresente = false;

            // Parcours les elements en entrée de la chaine (element dont la chaine dépend)
            for (ChaineProd c : this.listeChainesCritiques) {
                int i = Controleur.planningChaine.size();
                boolean trouve = false;

                // On parcours le planning des chaines en partant de la fin et on retire le nombre d'heure nécessaire  i sera alors l'heure minimale à laquelle la chaine pourra démarer
                while (i > 0 && !trouve) {
                    // Si la chaine critique est présente dans le cycle en cours de parcours
                    if (Controleur.planningChaine.get(i).contains(c)) {
                        trouve = true;
                    }

                    // Test si la chaine n'est pas critique a elle même
                    if (Controleur.planningChaine.get(i).contains(this)) {
                        trouve = true;
                        chainePresente = true;
                    }

                    // Si une chaine critique a été detectée à cette heure de fonctionnement
                    if (!trouve) {
                        i--;
                    }
                }
                // Si l'heure du dernier cycle de la chaine critique est supérieur à l'heure de démarage d'une autre chaine critique alors
                // l'heure de démarage recule en conséquence
                if (i > heureDemarage) {
                    heureDemarage = i;
                }
            }

            // Si la chaine n'est pas déjà présente dans le planning alors on peut l'ajouter à la suite des chaines qui lui sont critiques
            if (!chainePresente) {
                // Ajout de la chaine au planning à partir de son heure de démarage minimal
                int nbHeureDeTravaille = this.temps.get() * this.niveau.get();
                for (int i = heureDemarage + 1; i <= nbHeureDeTravaille + heureDemarage; i++) {
                    Controleur.planningChaine.get(i).add(this);
                }
            }
            // Si la chaine est déjà présente dans le planning alors on peut elle est critique à elle même et peut démarer un nouveau cycle seulement après avoir terminé le précédent
            else {
                int nbHeureDeTravaille = this.temps.get() * (this.niveau.get() - this.oldNiveau);
                for (int i = heureDemarage + 1; i <= nbHeureDeTravaille + heureDemarage; i++) {
                    Controleur.planningChaine.get(i).add(this);
                }
            }
        }
    }


    /**
     * Calcule l'heure de fin de cycle de la chaine et met à jour pour le personnel qui travail sur cette chaine la prochaine prose de poste possible pour chacun d'entre eux (prochainePrisePoste)
     */
    private void heureFinCycle() {
        // Parcours le planning des chaines jusqu'a trouver la chaine
        int hFinCycle = Controleur.planningChaine.size();
        boolean trouve = false;

        // On parcours le planning des chaines en partant de la fin et on retire le nombre d'heure nécessaire i sera alors l'heure de fin du cycle de la chaine en cours
        while (hFinCycle > 0 && !trouve) {
            // Si la chaine critique est présente dans le cycle en cours de parcours
            if (Controleur.planningChaine.get(hFinCycle).contains(this)) {
                trouve = true;
            }

            // Si la chaine n'a pas été trouvé on décrémente le compteur
            if (!trouve) {
                hFinCycle--;
            }
        }

        // Intialise l'heure de fin de cycle de la chaine
        this.hFinCycle = hFinCycle;

        // Ajoute au personnel l'heure à partir de laquelle il pourra travailler sur la meme ou une autre chaine
        for (Personnel p : Controleur.listePersonnel) {
            if (p.getPlanning().containsKey(this)) {
                p.setProchainePrisePoste(hFinCycle);
            }
        }
    }

}