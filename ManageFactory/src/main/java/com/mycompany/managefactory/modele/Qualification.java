package com.mycompany.managefactory.modele;

/**
 * Enumération de la Qualification possible pour un Personnel de l'usine
 */
public enum Qualification {
    QUALIFIE,
    NON_QUALIFIE
}
