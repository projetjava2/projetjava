package com.mycompany.managefactory.modele;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Classe d'un objet Element de l'usine
 */
public class Element {

    // *********************************************
    // ***************** ATTRIBUTS *****************
    // *********************************************

    private StringProperty code;
    private StringProperty nom;
    private IntegerProperty qte;
    private ObjectProperty<Unite> unite;
    private DoubleProperty pAchat;



    // ****************************************************
    // ******************* CONSTRUCTEUR *******************
    // ****************************************************

    /**
     * Constructeur d'un objet Element
     * @param code code d'identifiacation de l'élément
     * @param nom nom de l'élément
     * @param qte quantité en stock de l'élement
     * @param unite unité de mesure du stock de cet élément
     * @param pAchat prix d'achat de l'élément
     */
    public Element(String code, String nom, int qte, Unite unite, double pAchat) {
        this.code = new SimpleStringProperty(code);
        this.nom = new SimpleStringProperty(nom);
        this.qte = new SimpleIntegerProperty(qte);
        this.unite = new SimpleObjectProperty<>(unite);
        this.pAchat = new SimpleDoubleProperty(pAchat);
    }



    // *******************************************************
    // ******************* GETTER / SETTER *******************
    // *******************************************************

    /**
     * Retourne l'entier qui correspond au code de l'Element
     * @return Code de l'Element
     */
    public String getCode() {
        return this.code.get();
    }


    /**
     * Retourne la chaîne de caractère qui correspond au code de l'Element
     * @return Nom de l'Element
     */
    public String getNom() {
        return this.nom.get();
    }


    /**
     * Retourne l'entier qui correspond à la quantité en stock de l'Element
     * @return Quantité en stock de l'Element
     */
    public int getQte() {
        return this.qte.get();
    }


    /**
     * Retourne l'Unite de mesure de l'Element
     * @return Unite de l'Element
     */
    public Unite getUnite() {
        return this.unite.get();
    }


    /**
     * Retourne le réel (double) qui correspond au prix d'achat de l'Element
     * @return Prix d'achat de l'Element
     */
    public double getpAchat() {
        return pAchat.get();
    }


    /**
     * Retourne la PROPERTY chaîne de caractère qui correspond au Code de l'Element
     * @return PROPERTY Code de l'Element
     */
    public StringProperty getCodeProperty() {
    	return this.code;
    }


    /**
     * Retourne la PROPERTY chaîne de caractère qui correspond au Nom de l'Element
     * @return PROPERTY Nom de l'Element
     */
    public StringProperty getNomProperty() {
    	return this.nom;
    }


    /**
     * Permet de modifier la quantité en stock de l'Element
     * @param qte Nouvelle quantité en stock de l'Element
     */
    public void setQte(int qte) {
        this.qte.set(qte);
    }

}
