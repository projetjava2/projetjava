package com.mycompany.managefactory.modele;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.ObservableList;
import java.util.HashMap;
import java.util.Map;

/**
 * Classe d'un objet Personnel de l'usine
 */
public class Personnel {

    // *********************************************
    // ***************** ATTRIBUTS *****************
    // *********************************************

    private IntegerProperty num;
    private Qualification qualif;
    private int nbHeuresMax;
    private HashMap<ChaineProd, Integer> planning;
    private int prochainePrisePoste;



    // *****************************************************
    // ******************* CONSTRUCTEURS *******************
    // *****************************************************

    /**
     * Constructeur d'un objet personnel de l'usine
     * @param num Numéro d'identification de l'employé
     * @param qualif Statut de qualification de l'employé
     * @param nbHeuresMax Nombre d'heures maximales que peut travailler l'employé pour une semaine
     */
    public Personnel(int num, Qualification qualif, int nbHeuresMax) {
        this.num = new SimpleIntegerProperty(num);
        this.qualif = qualif;
        this.nbHeuresMax = nbHeuresMax;
        this.planning = new HashMap<> ();
        this.prochainePrisePoste = 0;
    }


    /**
     * Surcharge du constructeur d'un objet personnel de l'usine qui permet de créer un Personnel avec un planning déjà construit
     * @param num Numéro d'identification de l'employé
     * @param qualif Statut de qualification de l'employé
     * @param nbHeures Nombre d'heures maximales que peut travailler l'employé pour une semaine
     * @param planning Le planning de l'employé
     */
    public Personnel(int num, Qualification qualif, int nbHeures, HashMap<ChaineProd, Integer> planning) {
        // Chainnage vers le premier constructeur
        this(num, qualif, nbHeures);
        this.planning = planning;
    }



    // *******************************************************
    // ******************* GETTER / SETTER *******************
    // *******************************************************

    /**
     * Retourne le nombre d'heures hebdomadaires que peut réaliser un employé
     * @return Nombre d'heures maximum par semaine de l'employé
     */
    public int getNbHeuresMax(){
        return this.nbHeuresMax;
    }


    /**
     * Retourne la Qualification d'un employé
     * @return Qualification de l'employé
     */
    public Qualification getQualif(){
        return this.qualif;
    }


    /**
     * Retourne la HashMap qui correspond au planning de l'employé avec le temps passé sur chaque chaine de production (clé : ChaineProd, valeur : entier qui représente le temps passé sur la chaine)
     * @return HashMap du planning de l'employé (clé : ChaineProd, valeur : entier qui représente le temps passé sur la chaine)
     */
    public HashMap<ChaineProd, Integer> getPlanning(){
        return this.planning;
    }


    /**
     * Retourne l'entier qui correspond au numéro d'un employé
     * @return Entier du numéro d'un employé
     */
    public int getNum() {
        return this.num.get();
    }


    /**
     * Retourne la PORPERTY de l'entier qui correspond au numéro d'un employé
     * @return PROPERTY Entier du numéro d'un employé
     */
    public IntegerProperty getNumProperty() {
        return this.num;
    }


    /**
     * Retroune l'heure ou l'employe peut recommencer à travailler sur une chaine de production
     * @return Heure de prise de poste
     */
    public int getProchainePrisePoste() {
        return this.prochainePrisePoste;
    }


    /**
     * Permet de modifier l'heure de l prise de poste d'un employé
     * @param prochainePrisePoste nouvelle heure de prise de poste (heure de fin de cycle de la chaine sur laquelle l'employé travail)
     */
    public void setProchainePrisePoste(int prochainePrisePoste) {
        this.prochainePrisePoste = prochainePrisePoste;
    }



    // **********************************************************
    // ******************* METHODES PUBLIQUES *******************
    // **********************************************************

    /**
     * Permet de connaître le nombre d'heure qu'effectue un employé sur une semaine en fonction de son planning
     * @return Entier qui correspond au nombre d'heures effectivement travaillées
     */
    public int nbHeureEffectuees(){
        int nbHeureEffectuees = 0;
        for(Map.Entry<ChaineProd,Integer> tache : this.planning.entrySet()){
            nbHeureEffectuees += tache.getValue();
        }
        return nbHeureEffectuees;
    }


    /**
     * Calcul le nombre d'heure restante pour un employé sur une semaine
     * @return Entier qui correspond au nombre d'heures restantes
     */
    public int nbHeureRestante() {
        return this.nbHeuresMax - this.nbHeureEffectuees();
    }


    /**
     * Met à jour le planning d'un employé en lui ajoutant une tâche (affectation d'une chaine de production à son planning)
     * @param c Chaine de production sur lequel l'employé travaille
     * @param nbH  Nombre d'heures effectuées sur cette chaîne de production
     */
    public void addChainePlanning(ChaineProd c, Integer nbH){
        // L'employé peut déjà avoir travaillé sur la chaîne en question
        if(this.planning.containsKey(c)){ // Si l'employé travaille déjà sur cette chaîne
            this.planning.replace(c, nbH);
        }
        else{
            this.planning.put(c, nbH);
        }
    }


    /**
     * Méthode surchargée permettant d'ajouter une tâche à un employé dont on connaît le numéro
     * @param listePersonnel Ensemble du personnel de l'usine
     * @param id Numéro de l'employé auquel on souhaite ajouter la tâche
     * @param c Chaine de production sur laquelle l'employé va travailler
     * @param nbH Nombre d'heures effectuées sur cette chaîne
     */
    public static void addChainePlanning(ObservableList<Personnel> listePersonnel, int id, ChaineProd c, Integer nbH){
        for(Personnel p : listePersonnel){
            if(p.getNum() == id){
                // On est sur le bon employé
                // On lui ajoute la tâche sur la chaine de production et le nombre d'heures passées en paramètres
                p.addChainePlanning(c, nbH);
            }
        }
    }


    /**
     * Permet pour un nombre d'heures données, de trouver un employé pouvant travailler en fonction de son planning
     * @param listePersonnel Ensemble du personnel de l'usine
     * @param nbH Nombre d'heures de disponibles recherchées
     * @return Entier qui correspond au numéro d'identification de l'employé, 0 si aucun employé dispo.
     */
    public static int persDispo (ObservableList<Personnel> listePersonnel, int nbH, Qualification q){
        for(Personnel p: listePersonnel){
            if(p.nbHeuresMax - p.nbHeureEffectuees() >= nbH && p.qualif == q){
                return p.getNum();
            }
        }

        return 0;
    }


    /**
     * Permet de récupérer un employé parmis l'ensemble des employés de l'usine en connaissant son numéro
     * @param id Numéro d'identifiant de l'employé
     * @param listePersonnel Ensemble du personnel de l'usine
     * @return Personnel qui correspond à l'employé recherché, null si le numéro d'identifiant ne correspond pas à un employé
     */
    public static Personnel getPersonne(int id, ObservableList<Personnel> listePersonnel){
        for(Personnel p : listePersonnel){
            if(p.getNum() == id){
                return p;
            }
        }
        return null;
    }

}