package com.mycompany.managefactory.modele;

/**
 * Enumération pour le choix de l'unité de stock d'un élément de production
 */
public enum Unite {
    kg,
    litre,
    unité,
    carton,
    aucune
}
