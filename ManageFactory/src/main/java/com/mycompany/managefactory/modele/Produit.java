package com.mycompany.managefactory.modele;

/**
 * Classe d'un objet Produit de l'usine (cette classe hérite de la classe Element)
 */
public class Produit extends Element {

    // *********************************************
    // ***************** ATTRIBUTS *****************
    // *********************************************
    
    private double pVente;



    // ****************************************************
    // ******************* CONSTRUCTEUR *******************
    // ****************************************************
    
    /**
     * Permet d'instancier un produit qui est un élément de production
     * @param pVente : Prix de vente du produit
     * @param code : correspond au code d'identification du produit
     * @param nom : nom d produit
     * @param qte : quantité en stock du produit
     * @param unite : unité de mesure de ce produit dans le stock
     * @param pAchat : prix d'achat du produit (peut être non valué)
     */
    public Produit(double pVente, String code, String nom, int qte, Unite unite, double pAchat) {
        super(code, nom, qte, unite, pAchat);
        this.pVente = pVente;
    }



    // **********************************************
    // ******************* GETTER *******************
    // **********************************************

    /**
     * Permet d'accéder au prix de vente du produit depuis une autre classe
     */
    public double getpVente() {
        return this.pVente;
    }

}
