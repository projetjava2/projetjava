package com.mycompany.managefactory.controleur;

import com.mycompany.managefactory.modele.Element;
import com.mycompany.managefactory.modele.Produit;
import javafx.fxml.FXML;
import javafx.scene.control.*;

/**
 * Classe Controleur de la vue VisualiserStock.fxml
 */
public class VisualiserStockControleur {

	// *********************************************
	// ***************** ATTRIBUTS *****************
	// *********************************************

	private Element unElem; // Objet qui pointera sur l'élement courant
	@FXML
	private TableView<Element> stockTable; //La table à gauche
	@FXML
	private TableColumn<Element, String> codeColumn;
	@FXML
	private TableColumn<Element, String> elementNomColumn;
	@FXML
	private Label codeLabel;
	@FXML
	private Label nomLabel;
	@FXML
	private Label uniteStockLabel;
	@FXML
	private Label prixAchatLabel;
	@FXML
	private Label prixVenteLabel;
	@FXML
	private Spinner quantiteSpinner;
	// Reference au main app, notemment pour obtenir l'observable list remplie
	private Controleur controleur;



	// **********************************************************
	// ******************* METHODES PUBLIQUES *******************
	// **********************************************************

	/**
	 * Est appellé par la main pour faire une référence à lui même.
	 * Remplis le tableau avec les données du CSV (récupéré dans la liste static du stock).
	 *
	 * @param controleur Objet controleur
	 */
	public void setControleur(Controleur controleur) {
		this.controleur = controleur;

		/* Ajoute les données de l'observable liste remplies (récupéré depuis le main app) à la table à gauche
		dans la vue*/
		stockTable.setItems(Controleur.listeStock);
	}


	/**
	 * Permet de modifier la quantité d'un élement en cliquant sur le bouton valider de la vue
	 */
	public void modificationElement() {
		this.unElem.setQte(Integer.parseInt(this.quantiteSpinner.getValue().toString()));
	}


	/**
	 * Permet de revenir à l'accueil depuis la vue stock
	 */
	public void showAccueilStock() {
    	this.controleur.initRootLayout();
    }



	// ********************************************************
	// ******************* METHODES PRIVEES *******************
	// ********************************************************

	/**
	 * Initialise le contrôleur de la vue VisualiserStock. C'est la première
	 * méthode qui est appellée lors d'un appel de la vue.
	 */
	@FXML
	private void initialize() {
		// Initialise la colonne du code avec les données du CSV.
		codeColumn.setCellValueFactory(cellData -> cellData.getValue().getCodeProperty());
		// Initialise la colonne du nom avec les données du CSV.
		elementNomColumn.setCellValueFactory(cellData -> cellData.getValue().getNomProperty());

		// Initialisation de l vue
		showStockDetails(null);

		stockTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> showStockDetails(newValue));
	}


	/**
	 * Remplis les champs du tableau à droite dans la vue (détails d'un élement).
	 * Si l'élement est null dans ce cas la vue n'affiche rien (champs vides).
	 * Dans le cas ou l'élement spécifié est un produit, on affiche également le prix de vente, sinon NA.
	 *
	 * @param elem Un élement (sur lequel l'utilisateur clique dans la vue).
	 */
	private void showStockDetails(Element elem) {
		if (elem != null) {
			this.unElem = elem; // Pointe sur l'élement courant pour pouvoir éditer les valeurs de l'objet plus tard
			codeLabel.setText(elem.getCode());
			nomLabel.setText(elem.getNom());
			uniteStockLabel.setText("" + elem.getUnite());
			prixAchatLabel.setText(Double.toString(elem.getpAchat()));

			//Si l'element est un produit affiche son prix de vente à l'écran, sinon affiche NA
			if (elem instanceof Produit && ((Produit) elem).getpVente() != 0) {
				prixVenteLabel.setText(Double.toString(((Produit) elem).getpVente()));
			} else {
				prixVenteLabel.setText("NA");
			}

			if (elem.getpAchat() != 0) {
				prixAchatLabel.setText(Double.toString(elem.getpAchat()));
			}
			else {
				prixAchatLabel.setText("NA");
			}

			SpinnerValueFactory<Integer> quantiteElem = new SpinnerValueFactory.IntegerSpinnerValueFactory(0,100000,elem.getQte());
			this.quantiteSpinner.setValueFactory(quantiteElem);

		} else {
			codeLabel.setText("");
			nomLabel.setText("");
			uniteStockLabel.setText("");
			prixAchatLabel.setText("");
			prixVenteLabel.setText("");
		}
	}


}
