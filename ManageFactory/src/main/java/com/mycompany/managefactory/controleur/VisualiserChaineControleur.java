package com.mycompany.managefactory.controleur;

import com.mycompany.managefactory.modele.ChaineProd;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import java.util.ArrayList;

/**
 * Classe Controleur de la vue VisualiserChaine.fxml
 */
public class VisualiserChaineControleur {

	// *********************************************
	// ***************** ATTRIBUTS *****************
	// *********************************************

	private final String PATH_ICON = "/icon/logoManageFactory.png";
    private Image iconApp = new Image(PATH_ICON);
	private ChaineProd uneChaine;
	@FXML
	private TableView<ChaineProd> chaineTable; // La table à gauche
	@FXML
	private TableColumn<ChaineProd, String> codeColumn;
	@FXML
	private TableColumn<ChaineProd, String> chaineNomColumn;
	@FXML
	private Label codeLabel;
	@FXML
	private Label nomLabel;
	@FXML
	private Label statutLabel;
	@FXML
	private Label tempsLabel;
	@FXML
	private Label personnelQualifLabel;
	@FXML
	private Label personnelNonQualifLabel;
	@FXML
	private Label statutAffectationPersoLabel;
	@FXML
	private Label waitLabel;
	@FXML
	private Spinner niveauActivationSpinner;

	// Reference au controleur, notemment pour obtenir l'observable list remplie
	private Controleur controleur;



	// **********************************************************
	// ******************* METHODES PUBLIQUES *******************
	// **********************************************************

	/**
	 * Permet de modifier le niveau d'une chaine en cliquant sur le bouton valider de la vue
	 */
	public void modificationChaine() {
		try {


			// La modification n'a lieu seulement si le niveau saisi est différent de l'actuel
			if (this.uneChaine.getNiveau() != (Integer) this.niveauActivationSpinner.getValue()) {

				// L'affectation ne peut avoir lieu seulement si le personnel a été chargé au préalable
				if (Controleur.listePersonnel.size() > 0) {

					// Change le niveau d'activation de la chaine
					this.uneChaine.setNiveau((Integer) this.niveauActivationSpinner.getValue());

					// Gestion de l'affichage du statut de la chaine
					if (this.uneChaine.isStatut()) {
						this.statutLabel.setTextFill(Color.GREEN);
					} else {
						this.statutLabel.setTextFill(Color.RED);
					}

					this.statutLabel.setText(this.uneChaine.getStatutProperty().get());

					// Gestion de l'affichage du statut du personnel
					if (this.uneChaine.getStatutAffectationPersonnel()) {
						this.statutAffectationPersoLabel.setTextFill(Color.GREEN);
					} else {
						this.statutAffectationPersoLabel.setTextFill(Color.RED);
					}

					this.statutAffectationPersoLabel.setText(this.uneChaine.getStatutAffectationPersonnelProperty().get());
				} else {
					// Message d'alerte de non chargement du personnel
					Alert alert = new Alert(Alert.AlertType.WARNING);
					alert.setTitle("Attention - Affectation impossible");
					alert.setHeaderText("Absence de personnel");
					alert.setContentText("L'affectation du personnel ne peut avoir lieu car le personnel de l'usine n'a pas été importé.");
					Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
					stage.getIcons().add(iconApp);
					alert.showAndWait();
				}
			}
		}
		catch (Exception ex) {
			// Message d'alerte de dépassement de planning
			Alert alert = new Alert(Alert.AlertType.WARNING);
			alert.setTitle("Attention - Hors Planning");
			alert.setHeaderText("Plage horraire dépassée");
			alert.setContentText("L'attribution de la chaîne au planning n'est pas complète pour le niveau d'activation saisi.");
			Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
			stage.getIcons().add(iconApp);
			alert.showAndWait();
		}

		// Si la chaine est en attente
		this.labelChaineWait();
	}


	/**
	 * Permet de revenir à l'accueil depuis la vue chaîne
	 */
	public void showAccueilChaine() {
		this.controleur.initRootLayout();
	}


	/**
	 * Est appellé par la main pour faire une référence à lui même. Remplis le
	 * tableau avec les données du CSV (récupéré par getStockData du main app).
	 * @param controleur Objet controleur
	 */
	public void setControleur(Controleur controleur) {
		this.controleur = controleur;

		/*
		 * Ajoute les données de l'observable liste remplies (récupéré depuis le main
		 * app) à la table à gauche dans la vue
		 */
		chaineTable.setItems(Controleur.listeChaine);
	}



	// ********************************************************
	// ******************* METHODES PRIVEES *******************
	// ********************************************************

	/**
	 * Initialise le contrôleur de la vue VisualiserChaine. C'est la première
	 * méthode qui est appellée lors d'un appel de la vue.
	 */
	@FXML
	private void initialize() {
		// Initialise des colonnes du code avec les données
		codeColumn.setCellValueFactory(cellData -> cellData.getValue().getCodeProperty());
		chaineNomColumn.setCellValueFactory(cellData -> cellData.getValue().getNomProperty());

		showChaineDetails(null);

		chaineTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> showChaineDetails(newValue));
	}


	/**
	 * Remplis les champs du tableau à droite dans la vue (détails d'une chaîne).
	 * @param chaine Une chaine (sur lequel l'utilisateur clique dans la vue).
	 */
	private void showChaineDetails(ChaineProd chaine) {
		String isWorking = "ARRET";

		if (chaine != null) {
			this.uneChaine = chaine;
			this.codeLabel.setText(chaine.getCode());
			this.nomLabel.setText(chaine.getNom());

			// Définition du label du statut de la chaine
			if (chaine.isStatut()) {
				this.statutLabel.setTextFill(Color.GREEN);
				isWorking = "MARCHE";
			}
			else {
				this.statutLabel.setTextFill(Color.RED);
			}

			this.statutLabel.setText(isWorking);
			this.tempsLabel.setText(Integer.toString(chaine.getTemps()));
			this.personnelQualifLabel.setText(Integer.toString(chaine.getpQualifie()));
			this.personnelNonQualifLabel.setText(Integer.toString(chaine.getpNonQualifie()));

			// Définition du statutAffectationPersonnel du label de la chaine
			if (chaine.getStatutAffectationPersonnel()) {
				this.statutAffectationPersoLabel.setTextFill(Color.GREEN);
			}
			else {
				this.statutAffectationPersoLabel.setTextFill(Color.RED);
			}

			this.statutAffectationPersoLabel.setText(chaine.getStatutAffectationPersonnelProperty().get());
			
			SpinnerValueFactory<Integer> niveauActivation = new SpinnerValueFactory.IntegerSpinnerValueFactory(0,100000,chaine.getNiveau());
            this.niveauActivationSpinner.setValueFactory(niveauActivation);


			// Affiche un message si la chaine est en attente d'une autre chaine
			this.labelChaineWait();
		}
		else {
			this.codeLabel.setText("");
			this.nomLabel.setText("");
			this.statutLabel.setText("");
			this.tempsLabel.setText("");
			this.personnelQualifLabel.setText("");
			this.personnelNonQualifLabel.setText("");
			this.statutAffectationPersoLabel.setText("");
			this.waitLabel.setText("");
		}
	}


	/**
	 * Formate un message propre à la chaine si celle-ci a été ou est placée en attente
	 * Le message indique si il s'agit d'un problème de stock / de personnel / ou d'attente de chaine(s)
	 */
	private void labelChaineWait() {
		// Si la chaine est en attente
		if (Controleur.listeChaineEnAttente.contains(this.uneChaine)) {
			// Message des chaines bloqauntes
			StringBuilder chainesBloquantes = new StringBuilder();
			chainesBloquantes.append("En attente de : ");

			// Construction du message avec les chaines bloquantes
			int cpt = 1; // Permet de situer si on se trouve sur la dernière chaine
			ArrayList<ChaineProd> listeBloquante = this.uneChaine.chainesBloquantes();

			if (listeBloquante.size() > 0) {
				for (ChaineProd c : listeBloquante) {
					// Si on se trouve sur la dernière chaine
					if (cpt == listeBloquante.size()) {
						chainesBloquantes.append(c.getCode());
					}
					// Sinon le message n'est pas terminé donc il y aura d'autres chaines à ajouter
					else {
						chainesBloquantes.append(c.getCode()).append(" - ");
					}

					cpt++;
				}
			}
			else {
				if (this.uneChaine.getStatutProperty().get() == "MARCHE") {
					this.uneChaine.setStatut(false);
					chainesBloquantes.append("Personnel");
				}
				else {
					chainesBloquantes.append("Stock");
				}
			}

			// Initialisation du Label avec le message propre à la chaine en cours
			this.waitLabel.setText(chainesBloquantes.toString());
		}
		else {
			this.waitLabel.setText("");
		}
	}


}
