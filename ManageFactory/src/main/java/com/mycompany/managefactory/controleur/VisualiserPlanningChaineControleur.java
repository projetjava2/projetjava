package com.mycompany.managefactory.controleur;

import com.mycompany.managefactory.modele.ChaineProd;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Label;

/**
 * Classe Controleur de la vue VisualiserPlanningChaine.fxml
 */
public class VisualiserPlanningChaineControleur {

    // *********************************************
    // ***************** ATTRIBUTS *****************
    // *********************************************

    @FXML
    private TableView<Integer> heureTable; // La table à gauche
    @FXML
    private TableColumn<Integer, Number> heureColumn;
    @FXML
    private Label labelHeureCycle;
    @FXML
    private TableView<ChaineProd> chaineTable;
    @FXML
    private TableColumn<ChaineProd, String> chaineCodeColumn;
    @FXML
    private TableColumn<ChaineProd, String> chaineNomColumn;
    @FXML
    private TableColumn<ChaineProd, Number> chainePQColumn;
    @FXML
    private TableColumn<ChaineProd, Number> chainePnQColumn;
    // Reference au controleur, notemment pour obtenir l'observable list remplie
    private Controleur controleur;



    // **********************************************************
    // ******************* METHODES PUBLIQUES *******************
    // **********************************************************

    /**
     * Initialise l'objet Controleur
     * @param controleur Controleur de l'applciation
     */
    public void setControleur(Controleur controleur) {
        this.controleur = controleur;
        /*
         * Ajoute les données de l'observable liste remplies (récupéré depuis le main
         * app) à la table à gauche dans la vue
         */
        ObservableList<Integer> listeHeure = FXCollections.observableArrayList();
        listeHeure.setAll(Controleur.planningChaine.keySet());
        this.heureTable.setItems(listeHeure);
    }


    /**
     * Permet de revenir à l'accueil depuis la vue personnel
     */
    public void showAccueilPlanningChaine() {
        this.controleur.initRootLayout();
    }



    // ********************************************************
    // ******************* METHODES PRIVEES *******************
    // ********************************************************

    /**
     * Initialise le contrôleur de la vue VisualiserPersonnel. C'est la première
     * méthode qui est appellée lors d'un appel de la vue.
     */
    @FXML
    private void initialize() {
        // Initialise la colonne avec les données
        this.heureColumn.setCellValueFactory(cellData -> new SimpleIntegerProperty(cellData.getValue()));

        this.showHeureDetails(null);

        this.heureTable.getSelectionModel().selectedItemProperty().addListener((ChangeListener<Number>) (observable, oldValue, newValue) -> showHeureDetails(newValue.intValue()));
    }


    /**
     * Affiche le détail du plannig de l'heure sélectionné
     * @param h heure choisi
     */
    private void showHeureDetails(Integer h) {
        if (h != null) {
            this.labelHeureCycle.setText(h.toString());

            // Récupère les chaines appartenant au cycle de production sélectionné
            this.chaineTable.setItems(Controleur.planningChaine.get(h));

            // Affiche les chaines correspondant au cycle
            this.chaineCodeColumn.setCellValueFactory(cellData -> cellData.getValue().getCodeProperty());
            this.chaineNomColumn.setCellValueFactory(cellData -> cellData.getValue().getNomProperty());
            this.chainePQColumn.setCellValueFactory(cellData -> cellData.getValue().getPQualifieProperty());
            this.chainePnQColumn.setCellValueFactory(cellData -> cellData.getValue().getPNonQualifieProperty());
        }
        else {
            this.labelHeureCycle.setText("...");
        }
    }


}
