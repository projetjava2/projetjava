package com.mycompany.managefactory.controleur;

import com.mycompany.managefactory.modele.*;
import com.mycompany.managefactory.technique.PDF;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import java.io.File;
import java.util.Map;

/**
 * Classe Controleur de la vue VisualiserResultat.fxml
 */
public class VisualiserResultatControleur {

    // *********************************************
    // ***************** ATTRIBUTS *****************
    // *********************************************

    private Controleur controleur;
    private double ca;
    private double tauxCmdSatisfaites;
    @FXML
    private TableView<ChaineProd> chaineTable;
    @FXML
    private TableColumn<ChaineProd, String> codeColumn;
    @FXML
    private TableColumn<ChaineProd, String> nomColumn;
    @FXML
    private TableColumn<ChaineProd, Number> niveauColumn;
    @FXML
    private TableColumn<ChaineProd, String> statutColumn;
    @FXML
    private TableColumn<ChaineProd, Number> indicValColumn;
    @FXML
    private TableColumn<ChaineProd, Number> indicCmdColumn;
    @FXML
    private Label labelCA;
    @FXML
    private Label labelPourcentage;
    @FXML
    private Label labelHpQualif;
    @FXML
    private Label labelHpNonQualif;
    @FXML
    private ProgressBar progressBarCmd;



    // **********************************************************
    // ******************* METHODES PUBLIQUES *******************
    // **********************************************************

    /**
     * Initialise l'objet Controleur
     * @param controleur Controleur de l'applciation
     */
    public void setControleur(Controleur controleur) {
        this.controleur = controleur;
        this.chaineTable.setItems(Controleur.listeChaine);

        // Affichage du CA selon son état (positif ou négatif)
        this.ca = montantCA();
        // Modifie la couleur de l'affichage du CA en fonction de son état (positif / négatif)
        if (this.ca >= 0) {
            labelCA.setTextFill(Color.GREEN);
        }
        else {
            labelCA.setTextFill(Color.RED);
        }
        this.labelCA.setText(this.ca + " €");

        // Affichage du nombre d'heure restant du personnel
        int nbHeureRestantePersoQ = 0;
        int nbHeureRestantePersoNonQ = 0;

        if (Controleur.listePersonnel.size() > 0) {
            // Calcul du nombre total d'heure restante
            for (Personnel p : Controleur.listePersonnel) {
                if (p.getQualif().equals(Qualification.QUALIFIE)) {
                    nbHeureRestantePersoQ += p.nbHeureRestante();
                }
                else {
                    nbHeureRestantePersoNonQ += p.nbHeureRestante();
                }
            }
        }

        this.labelHpQualif.setText(nbHeureRestantePersoQ + " heures");
        this.labelHpNonQualif.setText(nbHeureRestantePersoNonQ + " heures");

        // Affichage du pourcentage des commandes satisfaites
        this.tauxCmdSatisfaites = this.tauxSatisfactionCommande();
        this.tauxCmdSatisfaites = Math.round(this.tauxCmdSatisfaites * 100.0)/100.0;
        this.labelPourcentage.setText(this.tauxCmdSatisfaites + " %");

        // Initialisation de la progressBar
        this.progressBarCmd.setProgress(this.tauxCmdSatisfaites/100);
    }


    /**
     * Permet de revenir à l'accueil depuis la vue résultat
     */
    public void showAccueilResultat() {
        this.controleur.initRootLayout();
    }



    // ********************************************************
    // ******************* METHODES PRIVEES *******************
    // ********************************************************

    /**
     * Initialise la vue Résultat
     */
    @FXML
    private void initialize() {
        // Initialiser les colonnes de la tableview avec les données
        this.codeColumn.setCellValueFactory(cellData -> cellData.getValue().getCodeProperty());
        this.nomColumn.setCellValueFactory(cellData -> cellData.getValue().getNomProperty());
        this.niveauColumn.setCellValueFactory(cellData -> cellData.getValue().getNiveauProperty());
        this.statutColumn.setCellValueFactory(cellData -> cellData.getValue().getStatutProperty());
        this.indicValColumn.setCellValueFactory(cellData -> cellData.getValue().getRentabiliteProperty());
        this.indicCmdColumn.setCellValueFactory(cellData -> cellData.getValue().demandeSatisfaite());
    }


    /**
     * Exporte les données (ChaineProd, Stock, Personnel) au format CSV dans le répertoire choisi
     */
    @FXML
    private void handleSaveAsCSV() {
        DirectoryChooser choixRepertoire = new DirectoryChooser();

        // Fenetre de sélection du répertoire
        File repertoire = choixRepertoire.showDialog(Controleur.primaryStage);

        if (repertoire != null) {
            File fichier = new File(repertoire.getPath());
            this.controleur.sauvegarderResultat(fichier);
        }

    }


    /**
     * Exporte les données au format PDF (édition de la vue résultat détaillée au format PDF)
     */
    @FXML
    private void handleSaveAsPDF() {
        try {
            FileChooser choixFichier = new FileChooser();

            // Ajout du filtre de l'extension
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PDF files (*.pdf)", "*.pdf");
            choixFichier.getExtensionFilters().add(extFilter);

            // Ouvre la fenêtre de dialogue pour déterminer le chemin d'enregistrement du fichier
            File fichier = choixFichier.showSaveDialog(Controleur.primaryStage);

            // Edition du pdf
            PDF.resultatPDF(fichier, this.ca, this.tauxCmdSatisfaites);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur d'enregistrement");
            alert.setHeaderText("Enregistrement impossible !");
            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            stage.getIcons().add(new Image("/icon/logoManageFactory.png"));
            alert.showAndWait();
        }
    }


    /**
     * Calcul le montant du chiffre d'affaire de l'usine et configure sa couleur suivant sa valeur (positive/négative)
     * @return un double du chiffre d'affaire
     */
    private Double montantCA() {
        double ca = 0;
        // Calcule le CA
        for (ChaineProd c : Controleur.listeChaine) {
            ca += c.rentabilite();
        }

        return ca;
    }


    /**
     * Permet de savoir si un élément à une demande (pour le calcul de l'indicateur de commande)
     * @param e Element en cours
     * @return vrai ou faux en fonction de l'existance d'une demande
     */
    private boolean aDemande(Element e){
        int nbDemande = 0;
        boolean res = false;
        for(Demande d : Controleur.listeDemande){
            if(e.getCode().equals(d.getElement().getCode())){ // On ne rentre qu'une seule fois dans le if
                nbDemande = d.getQteDem();
            }
        }
        if(nbDemande != 0){
            res = true;
        }
        return res;
    }
    
    
    /**
     * Permet de calculer le taux moyens de satisfaction des demandes
     * @return retourne le taux de satisfaction des commandes
     */
    private Double tauxSatisfactionCommande() {
        double taux = 0;
        int i = 0;
        for (ChaineProd c : Controleur.listeChaine) {
            taux += c.demandeSatisfaite().get();
            for(Map.Entry<Produit, Integer> p : c.getListeSortie().entrySet()){
                if(this.aDemande(p.getKey())){
                    i++ ; // On ne prend en compte pour le calcul que les éléments pour lesquels il y a une demande
                }
            }
        }
        return taux/i;
    }

}
