package com.mycompany.managefactory.controleur;

import com.mycompany.managefactory.modele.Demande;
import com.mycompany.managefactory.modele.Produit;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

/**
 * Classe Controleur de la vue VisualiserDemande.fxml
 */
public class VisualiserDemandeControleur {

	// *********************************************
	// ***************** ATTRIBUTS *****************
	// *********************************************

	@FXML
	private TableView<Demande> demandeTable; // La table à gauche
	@FXML
	private TableColumn<Demande, String> codeColumn;
	@FXML
	private TableColumn<Demande, String> demandeNomColumn;
	@FXML
	private TableColumn<Demande, Number> quantiteDemandeColumn;
	@FXML
	private Label codeLabel;
	@FXML
	private Label nomLabel;
	@FXML
	private Label quantiteStockLabel;
	@FXML
	private Label prixVenteLabel;
	@FXML
	private Label quantiteDemandeLabel;
	// Reference au main app, notemment pour obtenir l'observable list remplie
	private Controleur controleur;



	// **********************************************************
	// ******************* METHODES PUBLIQUES *******************
	// **********************************************************

	/**
	 * Est appellé par la main pour faire une référence à lui même. Remplis le
	 * tableau avec les données du CSV (récupéré par getDemandeData du main app).
	 * @param controleur Objet controleur
	 */
	public void setControleur(Controleur controleur) {
		this.controleur = controleur;

		 // Ajoute les données de l'observable liste remplies (récupéré depuis le maiapp) à la table à gauche dans la vue
		demandeTable.setItems(Controleur.listeDemande);
	}


	/**
	 * Permet de revenir à l'accueil depuis la vue demande
	 */
	public void showAccueilDemande() {
		this.controleur.initRootLayout();
	}



	// ********************************************************
	// ******************* METHODES PRIVEES *******************
	// ********************************************************

	/**
	 * Initialise le contrôleur de la vue VisualiserDemande.
	 * C'est la première méthode qui est appellée lors d'un appel de la vue
	 */
	@FXML
	private void initialize() {
		// Initialise les colonnes avec les données
		codeColumn.setCellValueFactory(cellData -> cellData.getValue().getElement().getCodeProperty());
		demandeNomColumn.setCellValueFactory(cellData -> cellData.getValue().getElement().getNomProperty());
		quantiteDemandeColumn.setCellValueFactory(cellData -> cellData.getValue().getQteDemProperty());

		showDemandeDetails(null);

		demandeTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> showDemandeDetails(newValue));
	}


	/**
	 * Affiche le détail de la demande
	 * @param dem Objet demande dont on souhaite obtenir le détail
	 */
	private void showDemandeDetails(Demande dem) {
		if (dem != null) {
			codeLabel.setText(dem.getElement().getCode());
			nomLabel.setText(dem.getElement().getNom());
			quantiteStockLabel.setText(Integer.toString(dem.getElement().getQte()));
			
			 //Si l'element est un produit affiche son prix de vente à l'écran, sinon affiche NA
	           if (dem.getElement() instanceof Produit) {
	        	   prixVenteLabel.setText(Double.toString(((Produit) dem.getElement()).getpVente()));
	           } else {
	        	   prixVenteLabel.setText("NA");
	           }

			this.quantiteDemandeLabel.setText(Integer.toString(dem.getQteDem()));
		}
		else {
			codeLabel.setText("");
			nomLabel.setText("");
			quantiteStockLabel.setText("");
			prixVenteLabel.setText("");
			quantiteDemandeLabel.setText("");
		}
	}
	
}
