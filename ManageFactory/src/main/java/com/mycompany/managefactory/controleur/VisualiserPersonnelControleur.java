package com.mycompany.managefactory.controleur;

import com.mycompany.managefactory.modele.ChaineProd;
import com.mycompany.managefactory.modele.Personnel;
import com.mycompany.managefactory.modele.Qualification;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.paint.Color;
import java.util.Map;

/**
 * Classe Controleur de la vue VisualiserPersonne.fxml
 */
public class VisualiserPersonnelControleur {

    // *********************************************
	// ***************** ATTRIBUTS *****************
	// *********************************************

	@FXML
	// La table à gauche
	private TableView<Personnel> tablePersonnel;
	@FXML
	private TableColumn<Personnel, Number> numColumn;
	@FXML
	private Label numeroP;
	@FXML
	private Label qualifP;
	@FXML
	private Label nbHeureP;
	@FXML
	private Label labelNbHeureTravaille;
	@FXML
	private Label labelNbHeureRestante;
	@FXML
	private TableView<Map.Entry<ChaineProd,Integer>> tableChaine;
	@FXML
	TableColumn<Map.Entry<ChaineProd, Integer>, String> codeColumn;
	@FXML
	TableColumn<Map.Entry<ChaineProd, Integer>, String> nomColumn;
	@FXML
	TableColumn<Map.Entry<ChaineProd, Integer>, String> nbHColumn;
	// Reference au main app, notemment pour obtenir l'observable list remplie
	private Controleur controleur;



	// **********************************************************
	// ******************* METHODES PUBLIQUES *******************
	// **********************************************************

	/**
	 * Initialise l'objet Controleur
	 * @param controleur Controleur de l'applciation
	 */
	public void setControleur(Controleur controleur) {
		this.controleur = controleur;
		// Ajoute les données de la collection observable static du personnel du Controleur
		tablePersonnel.setItems(Controleur.listePersonnel);
	}


	/**
	 * Permet de revenir à l'accueil depuis la vue personnel
	 */
	public void showAccueilPersonnel() {
		this.controleur.initRootLayout();
	}



	// ********************************************************
	// ******************* METHODES PRIVEES *******************
	// ********************************************************

	/**
	 * Initialise le contrôleur de la vue VisualiserPersonnel. C'est la première
	 * méthode qui est appellée lors d'un appel de la vue.
	 */
	@FXML
	private void initialize() {
		// Initialise la colonne avec les données
		numColumn.setCellValueFactory(cellData -> cellData.getValue().getNumProperty());

		showPersonnelDetails(null);

		tablePersonnel.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> showPersonnelDetails(newValue));
	}


	/**
	 * Affiche le détail du personnel sélectionné
	 * @param p personnel choisi
	 */
	private void showPersonnelDetails(Personnel p) {
		if (p != null) {

			this.numeroP.setText(Integer.toString(p.getNum()));

			if (p.getQualif() == Qualification.QUALIFIE) {
				this.qualifP.setTextFill(Color.BLUE);
			}
			else {
				this.qualifP.setTextFill(Color.ORANGE);
			}
			this.qualifP.setText(p.getQualif().toString());
			this.nbHeureP.setText(Integer.toString(p.getNbHeuresMax()));
			this.labelNbHeureTravaille.setText(p.nbHeureEffectuees() + " heures");
			this.labelNbHeureRestante.setText(p.nbHeureRestante() + " heures");
			this.tableChaine.setItems(FXCollections.observableArrayList(p.getPlanning().entrySet()));
			this.codeColumn.setCellValueFactory(cellData -> cellData.getValue().getKey().getCodeProperty());
			this.nomColumn.setCellValueFactory(cellData -> cellData.getValue().getKey().getNomProperty());
			this.nbHColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getValue().toString()));
		}
		else {
			this.numeroP.setText("");
			this.qualifP.setText("");
			this.nbHeureP.setText("");
			this.labelNbHeureTravaille.setText("");
			this.labelNbHeureRestante.setText("");
		}
	}

}
