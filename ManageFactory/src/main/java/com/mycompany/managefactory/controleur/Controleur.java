package com.mycompany.managefactory.controleur;

import com.mycompany.managefactory.modele.*;
import com.mycompany.managefactory.technique.CSV_file;
import com.mycompany.managefactory.technique.DonneesStrategy;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import java.io.File;
import java.io.IOException;
import java.util.*;


/**
 * Classe Controleur de l'application ManageFactory
 */
public class Controleur extends Application {

    // ***********************************************************
    // ***************** ATTRIBUTS ET CONSTANTES *****************
    // ***********************************************************

    // Constantes des chemins
	private final String PATH_POINT = "/icon/pointV.png";
    private final String PATH_ICON = "/icon/logoManageFactory.png";
    private final String PATH_SCENEFXML = "/fxml/Scene.fxml";
    private final String PATH_VISUALISERSTOCKFXML = "/fxml/VisualiserStock.fxml";
    private final String PATH_VISUALISERCHAINEFXML = "/fxml/VisualiserChaine.fxml";
    private final String PATH_VISUALISERDEMANDEFXML = "/fxml/VisualiserDemande.fxml";
    private final String PATH_VISUALISERRESULTATFXML = "/fxml/VisualiserResultat.fxml";
    private final String PATH_VISUALISERPERSONNELFXML = "/fxml/VisualiserPersonnel.fxml";
    private final String PATH_VISUALISERPLANNINGCHAINES = "/fxml/VisualiserPlanningChaine.fxml";
    private final static int TEMPS_MAX = 60;
    
	// Stage et Borderpane
    public static Stage primaryStage; //Container principal
    public ImageView logo;
    private AnchorPane rootLayout;
    private Image iconApp = new Image(PATH_ICON);
    private Image pointVert = new Image(PATH_POINT);

    // Les points de couleurs de la vue scene.fxml
    @FXML
    private ImageView pointStock;
    @FXML
    private ImageView pointChaine;
    @FXML
    private ImageView pointDemande;
    @FXML
    private ImageView pointPersonnel;

    // Déclaration de la liste observable
    public static ObservableList<Element> listeStock = FXCollections.observableArrayList();
    public static ObservableList<ChaineProd> listeChaine = FXCollections.observableArrayList();
    public static ObservableList<Demande> listeDemande = FXCollections.observableArrayList();
    public static ObservableList<Personnel> listePersonnel = FXCollections.observableArrayList();
    public static Map<Integer, ObservableList<ChaineProd>> planningChaine = new HashMap<>();
    public static Set<ChaineProd> listeChaineEnAttente = new HashSet<>();

    // Source de données
    private DonneesStrategy sourceDonnees;



    // ************************************************
    // ***************** CONSTRUCTEUR *****************
    // ************************************************
   
    /**
     * Contructeur qui initialise la source de données
     */
    public Controleur() {
    	try {
    		// Chargement des données dans les listes observables selon la source de données
            // Pour le moment qu'une seule source de données est gérée, donc pas de test ni demande à effectuer sur le choix de la source de données
            this.sourceDonnees = new CSV_file();

            // Initialisation du planning des chaines pour une semaine suivant le temps maximun de production de la semaine seulement au lancement
            if (planningChaine.isEmpty()) {
                for (int i = 1; i <= TEMPS_MAX; i++) {
                    ObservableList<ChaineProd> listeChaine = FXCollections.observableArrayList();
                    planningChaine.put(i, listeChaine);
                }
            }
        }
    	catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Initialisation de Scene.fxml.
     */
    @FXML
    public void initialize() {
        // Indicateur de chargement des fichiers de données
        if (listeStock.size() > 0) {
        	this.pointStock.setImage(pointVert);
        }
        if (listeChaine.size() > 0) {
        	this.pointChaine.setImage(pointVert);
        }
        if (listeDemande.size() > 0) {
        	this.pointDemande.setImage(pointVert);
        }
        if (listePersonnel.size() > 0) {
        	this.pointPersonnel.setImage(pointVert);
        }
    }



    // ************************************************
    // ************* AFFICHAGE DES ECRANS *************
    // ************************************************

    /**
     * Initialise le root layout (scene.fxml), qui est l'écran de projection principale de l'application.
     */
    public void initRootLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Controleur.class.getResource(PATH_SCENEFXML));
            this.rootLayout = loader.load();

            //Création d'une scène (arrière plan pour les élements de l'UI)
            Scene scene = new Scene(rootLayout);
               
            //On affecte au stage (container principal) la scène
            primaryStage.setScene(scene);
            primaryStage.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Affiche la vue stock à l'écran lors du lancement de l'application
     * @throws IOException Erreur d'affichage
     */
    public void showStock() throws IOException {	
    	FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Controleur.class.getResource(PATH_VISUALISERSTOCKFXML));
        AnchorPane page = loader.load();

        Scene scene = new Scene(page);
        primaryStage.setScene(scene);

        VisualiserStockControleur controller = loader.getController();
        controller.setControleur(this);      
    }
    

    /**
     * Affiche la vue chaine
     * @throws IOException Erreur d'affichage
     */
    public void showListeChaine() throws IOException  {
    	FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Controleur.class.getResource(PATH_VISUALISERCHAINEFXML));
        AnchorPane page = loader.load();
        
        Scene scene = new Scene(page);   
        primaryStage.setScene(scene);

        VisualiserChaineControleur controller = loader.getController();
        controller.setControleur(this);
    }


    /**
     * Affiche la vue demande
     * @throws IOException Erreur d'affichage
     */
    public void showDemande() throws IOException  {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Controleur.class.getResource(PATH_VISUALISERDEMANDEFXML));
        AnchorPane page = loader.load();
   
        Scene scene = new Scene(page);     
        primaryStage.setScene(scene);

        VisualiserDemandeControleur controller = loader.getController();
        controller.setControleur(this);

    }


    /**
     * Affiche la vue visualiserResultat
     * @throws IOException Erreur d'affichage
     */
    public void showResultat() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Controleur.class.getResource(PATH_VISUALISERRESULTATFXML));
        AnchorPane page = loader.load();

        Scene scene = new Scene(page);   
        primaryStage.setScene(scene);

        VisualiserResultatControleur controller = loader.getController();
        controller.setControleur(this);        
    }
    
    
    /**
     * Affiche la vue VisualiserPersonnel
     * @throws IOException Erreur d'affichage
     */
    public void showPersonnel() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Controleur.class.getResource(PATH_VISUALISERPERSONNELFXML));
        AnchorPane page = loader.load();

        Scene scene = new Scene(page);
        primaryStage.setScene(scene);

        VisualiserPersonnelControleur controller = loader.getController();
        controller.setControleur(this);
    }


    /**
     * Affiche la vue VisualiserPersonnel
     * @throws IOException Erreur d'affichage
     */
    public void showPlanningChaine() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Controleur.class.getResource(PATH_VISUALISERPLANNINGCHAINES));
        AnchorPane page = loader.load();

        Scene scene = new Scene(page);
        primaryStage.setScene(scene);

        VisualiserPlanningChaineControleur controller = loader.getController();
        controller.setControleur(this);
    }



    // ************************************************
    // ******************* METHODES *******************
    // ************************************************

    /**
     * Permet d'enregistrer le resultat à l'emplacement du fichier souhaité par l'utiulisateur
     * @param repertoire Emplacement d'enregistrement
     */
    public void sauvegarderResultat(File repertoire) {
        try {
            // L'export des chaines n'est faisable seulement si le personnel a été importé précédement
            if (listeChaine.size() > 0) {
                this.sourceDonnees.exportChaine(repertoire);
            }

            // L'export du stock n'est faisable seulement si le personnel a été importé précédement
            if (listeStock.size() > 0) {
                this.sourceDonnees.exportStock(repertoire);
            }

            // L'export du personnel n'est faisable seulement si le personnel a été importé précédement
            if (listePersonnel.size() > 0) {
                this.sourceDonnees.exportPersonnel(repertoire);
            }

            // Message d'alerte de confirmation de l'enregistrement des fichiers à l'emplacement souhaité
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Exportation réussi");
            alert.setHeaderText("Exportation réussi");
            alert.setContentText("Localisation : \n" + repertoire.getPath());
            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            stage.getIcons().add(iconApp);
            alert.showAndWait();
        }
        catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("ERREUR");
            alert.setHeaderText("Enregistrement impossible");
            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            stage.getIcons().add(iconApp);
            alert.showAndWait();
        }
    }


    /**
     * Import du stock depuis un fichier choisi par l'utilisateur
     */
    public void importerStock() {
        try {
            listeStock.setAll(this.sourceDonnees.loadElement());
            if (listeStock.size() > 0) {
            	this.pointStock.setImage(pointVert);
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur de chargement");
            alert.setHeaderText("Chargement du stock impossible !");
            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            stage.getIcons().add(iconApp);
            alert.showAndWait();
        }
    }


    /**
     * Import des chaines de production depuis un fichier choisi par l'utilisateur
     */
    public void importerChaineProd() {
        try {
            if (listeStock.size() != 0) {
                listeChaine.setAll(this.sourceDonnees.loadChaine());
                // Initialisation des dépendances des chaines de production
                for (ChaineProd c : listeChaine) {
                    c.chainesCritiques();
                    c.chainesDependances();
                }

                if (listeChaine.size() > 0) {
                	 this.pointChaine.setImage(pointVert);
                }
            }
            else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Chargement des chaines de production");
                alert.setContentText("Pour charger des chaines de production, il est nécessaire d'avoir déjà chargé le stock.");
                Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                stage.getIcons().add(iconApp);
                alert.showAndWait();
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur de chargement");
            alert.setHeaderText("Chargement des chaines impossible !");
            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            stage.getIcons().add(iconApp);
            alert.showAndWait();
        }
    }


    /**
     * Import des demandes depuis un fichier choisi par l'utilisateur
     */
    public void importerDemande() {
        try {
            if (listeStock.size() != 0) {
                listeDemande.setAll(this.sourceDonnees.loadDemande());
                if (listeDemande.size() > 0) {
                    this.pointDemande.setImage(pointVert);
                }
            }
            else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Chargement des demandes");
                alert.setContentText("Pour charger des demandes, il est nécessaire d'avoir déjà chargé le stock.");
                Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                stage.getIcons().add(iconApp);
                alert.showAndWait();
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur de chargement");
            alert.setHeaderText("Chargement des demandes impossible !");
            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            stage.getIcons().add(iconApp);
            alert.showAndWait();
        }
    }


    /**
     * Impot du personnel depuis un fichier choisi par l'utilisateur
     */
    public void importerPersonnel() {
        try {
            listePersonnel.setAll(this.sourceDonnees.loadPersonnel());

            if (listePersonnel.size() > 0) {
                this.pointPersonnel.setImage(pointVert);
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur de chargement");
            alert.setHeaderText("Chargement du personnel impossible !");
            alert.setContentText("Si votre personnel possède un planning, il est nécessaire de charger les chaines de production au préalable.");
            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            stage.getIcons().add(iconApp);
            alert.showAndWait();
        }
    }



    // ************************************************
    // ******************* DEMARAGE *******************
    // ************************************************

    /**
     * Procedure qui permet de démarrer l'application
     * @param stage stage
     */
    @Override
    public void start(Stage stage) {
    	// Création du stage (container) principal
    	primaryStage = stage;

        // Affectation d'un titre au stage
        primaryStage.setTitle("Manage Factory");

        // Affectation du logo à l'application
        primaryStage.getIcons().add(this.iconApp);

        //Initialisation du layout
        this.initRootLayout();
    }


    /**
     * Point d'entrée de l'application (lancement de l'application)
     * @param args arguments du main
     */
    public static void main(String[] args) {
        try {
            launch(args);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("ERREUR");
            alert.setHeaderText("Lancement impossible !");
            alert.showAndWait();
        }
    }
   
}
